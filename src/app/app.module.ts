import { NetService } from './net.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import {Router} from '@angular/router';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { MoviecardsComponent } from './moviecards/moviecards.component';
import { SongcardsComponent } from './songcards/songcards.component';
import { CompetitionComponent } from './competition/competition.component';
import { StudentComponent } from './student/student.component';
import { SocialMediaPostComponentComponent } from './social-media-post-component/social-media-post-component.component';
import { GetproductComponent } from './getproduct/getproduct.component';
import { LeftpanelComponent } from './leftpanel/leftpanel.component';
import { BookComponent } from './book/book.component';
import { PicViewerComponent } from './pic-viewer/pic-viewer.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { CellComponent } from './cell/cell.component';
import { MobileComponent } from './mobile/mobile.component';
import { ProductComponent } from './product/product.component';
import { CartNavBarComponent } from './cart-nav-bar/cart-nav-bar.component';
import { FormComponent } from './form/form.component';
import { StudentListComponent } from './student-list/student-list.component';
import { ChoosenamesComponent } from './choosenames/choosenames.component';
import { QuestionComponent } from './question/question.component';
import { MarkSheetComponent } from './mark-sheet/mark-sheet.component';
import {HttpClientModule} from '@angular/common/http';
import { HelloComponent } from './hello/hello.component';
import { ByeComponent } from './bye/bye.component';
import { HomeComponent } from './home/home.component';
import { NewMatchComponent } from './new-match/new-match.component';
import { MatchComponent } from './match/match.component';
import { AllMatchesComponent } from './all-matches/all-matches.component';
import { PointsTableComponent } from './points-table/points-table.component';
import { TeamComponent } from './team/team.component';
import { TeamoptionsComponent } from './teamoptions/teamoptions.component';
import { MatchXComponent } from './match-x/match-x.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AddNewProductComponent } from './add-new-product/add-new-product.component';
import { ReceiveStockComponent } from './receive-stock/receive-stock.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { QuestionFormComponent } from './question-form/question-form.component';
import { BooksComponent } from './books/books.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeOptionsComponent } from './employee-options/employee-options.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviecardsComponent,
    SongcardsComponent,
    CompetitionComponent,
    StudentComponent,
    SocialMediaPostComponentComponent,
    GetproductComponent,
    LeftpanelComponent,
    BookComponent,
    PicViewerComponent,
    FavouritesComponent,
    CellComponent,
    MobileComponent,
    ProductComponent,
    CartNavBarComponent,
    FormComponent,
    StudentListComponent,
    ChoosenamesComponent,
    QuestionComponent,
    MarkSheetComponent,
    HelloComponent,
    ByeComponent,
    HomeComponent,
    NewMatchComponent,
    MatchComponent,
    AllMatchesComponent,
    PointsTableComponent,
    TeamComponent,
    TeamoptionsComponent,
    MatchXComponent,
    LoginComponent,
    LogoutComponent,
    AddNewProductComponent,
    ReceiveStockComponent,
    EditProductComponent,
    QuestionFormComponent,
    BooksComponent,
    EmployeesComponent,
    EmployeeOptionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
HttpClientModule
  ],
  providers: [NetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
