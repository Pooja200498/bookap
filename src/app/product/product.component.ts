import { Component, OnInit, Input, Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
@Input() prod;
@Output() changedvalue = new EventEmitter();
Property = true;
@Input()  condition;
  constructor() { }

increase(value) {
  this.prod.quantity = this.prod.quantity + value;
this.changedvalue.emit(this.prod );
}

decrease(value) {
  this.prod.quantity = this.prod.quantity + value;
if (this.prod.quantity === 0) {
this.Property = false;
}

this.changedvalue.emit(this.prod );
}

  ngOnInit() {
  }

}
