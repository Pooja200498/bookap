import { NetService } from './../net.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
url1 = 'http://localhost:2410/sporticons/stars';
url2 = '';

strData;
strData1;
data: any;
sports: string = null;


  //constructor(private authService : AuthService, netService: NetService) { }
  
  constructor(private  netService: NetService,private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.sports = params.get('sports')
    this.getData();
    });
    this.getData();

}

getData() {
  if (this.sports === null) {
    this.url2 = this.url1;
  }
  else {
    this.url2 = this.url1+'/'+this.sports;
  }
  console.log(this.url2);
this.netService.getData(this.url2)
.subscribe(resp => {
this.strData = JSON.stringify(resp);
this.data = resp;
});
}


    /*if (this.sports === null) {
      console.log(this.sports);
  this.netService.getData(this.url1)
.subscribe(resp => {
  console.log(resp);
this.strData = JSON.stringify(resp);
this.data = resp;
console.log(this.data);
});
}

else {
this.route.paramMap.subscribe(params => {
  this.sports = params.get('sports')
  console.log(this.sports);
  this.netService.getData(this.url1+'/'+this.sports)
  .subscribe(resp => {
    console.log(resp);
    this.data = resp;
    console.log(this.data);
    console.log(this.sports);
  });

    })
  }

}*/

/*details(n) {
this.id = n;
this.Property1 = false;
this.Property2 = true;
let path = "/details/" + this.id ;
let qparams = {};
qparams['id'] = this.id;
this.router.navigate([path], {queryParams: qparams});

}*/




  /*status:boolean = false;
username = '';*/

  /*constructor(private authService : AuthService) { }

  ngOnInit() {
    //this.status = this.authService.loginStatus;
    //this.username = this.authService.username;

  }*/

 /* visitedPages;
  newArray = [];

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.visitedPages = this.authService.visitedPages;
    this.visitedPages = this.visitedPages + 1;
    this.authService.changedData(this.visitedPages);
    this.authService.changedData1('Home');
    this.newArray = this.authService.visitedArray;
  }*/

}
