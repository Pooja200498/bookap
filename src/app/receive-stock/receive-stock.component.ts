import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-receive-stock',
  templateUrl: './receive-stock.component.html',
  styleUrls: ['./receive-stock.component.css']
})
export class ReceiveStockComponent implements OnInit {
@Input() products;
@Output() updatedData2 = new EventEmitter();
//@Output() updatedData3 = new EventEmitter();

prod = '';
qty;

submit() {
  if (this.prod === '') {
alert('Select the product');
  }
  else if(this.qty === undefined){
    alert('Enter the quantity');
  }
else{
for (let i = 0; i < this.products.length; i++) {
  if (this.products[i].code === this.prod) {
this.products[i].quantity = +(this.qty);
  }
}
this.updatedData2.emit();
}
}

goBackToHome() {
  this.updatedData2.emit();
}

  constructor() { }

  ngOnInit() {

  }

}
