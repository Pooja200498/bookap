import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchXComponent } from './match-x.component';

describe('MatchXComponent', () => {
  let component: MatchXComponent;
  let fixture: ComponentFixture<MatchXComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchXComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
