import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {


  /*playersArray = ['Nadal', 'Roger', 'Virat', 'Lionel', 'Jasprit', 'Cristiano'];
  sportsArray = ['Cricket', 'Football', 'Tennis'];
  team = 'TeamPQR';
  page = '';
players = '';
sport = '';
playersStructure = null;
sportsStructure = null;
playersSelected = '';
currPage = 1;
maxPage = 5;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
this.route.paramMap.subscribe(params => {
  this.team = params.get('team');
});

this.route.queryParamMap.subscribe(params => {
  this.page = params.get('page');
  this.players = params.get('players');
  this.sport = params.get('sport');
  this.makeStructs();
});

  }

  makeStructs() {
    this.currPage = this.page ? +this.page : 1;
    this.playersSelected = this.players ? this.players : "";
    this.playersStructure = this.playersArray.map(pl1 => ({
      name : pl1,
      selected: false
    }));
    let temp1 = this.playersSelected.split(",");
    for ( let i = 0; i < temp1.length; i++ ) {
let item = this.playersStructure.find(p1 => p1.name === temp1[i]);
if (item) item.selected = true;
    }
    this.sportsStructure = {
      sports: this.sportsArray,
      selected: this.sport ? this.sport : ""
    };
    console.log(this.playersStructure);
    console.log(this.sportsStructure);
  }

  optChange() {
    let temp1 = this.playersStructure.filter(p1 => p1.selected);
    let temp2 = temp1.map(p1 => p1.name);
    this.playersSelected = temp2.join(",");
    let path = "/team/" + this.team;
    let qparams = {};
    if (this.playersSelected)
    qparams['players'] = this.playersSelected;
    if (this.sportsStructure.selected)
    qparams["sport"] = this.sportsStructure.selected;
    this.router.navigate([path], { queryParams: qparams });
  }

  gotoPage(x) {
    this.currPage = this.currPage + x;
    let path = "/team/" + this.team;
    this.router.navigate([path], {
      queryParams: {
        page: this.currPage
      },
      queryParamsHandling: "merge"
    });
  }*/


  /*coursesArray = ['BTech', 'MTech', 'BCA', 'MCA', 'BSC', ];
    statusArray = ['Studying', 'Working', 'Searching', 'Preparing'];

  mainArray =  [
      {
      name: "Amit",
      course: "BTech",
      year: 2019,
      status: "Studying",
      tech: "React"
      },
      {
      name: "Praveen",
      course: "BSc",
      year: 2020,
      status: "Studying",
      tech: "Angular"
      },
      {
      name: "Namita",
      course: "MCA",
      year: 2021,
      status: "Studying",
      tech: "Android"
      },
      {
      name: "Anuradha",
      course: "MTech",
      year: 2019,
      status: "Studying",
      tech: "Android"
      },
      {
      name: "Kavita",
      course: "BCA",
      year: 2020,
      status: "Studying",
      tech: "React"
      },
      {
      name: "Manish",
      course: "BTech",
      year: 2016,
      status: "Working",
      tech: "React"
      },
      {
      name: "Gautam",
      course: "BTech",
      year: 2017,
      status: "Working",
      tech: "Angular"
      },
      {
      name: "Radhika",
      course: "MCA",
      year: 2016,
      status: "Working",
      tech: "React"
      },
      {
      name: "Charu",
      course: "MTech",
      year: 2018,
      status: "Searching",
      tech: "Android"
      },
      {
        name: "Divya",
        course: "BCA",
 year: 2019,
 status: "Preparing",
 tech: "Angular"
 },
 {
 name: "Pradeep",
 course: "BTech",
 year: 2016,
 status: "Working",
 tech: "React"
 },
 {
 name: "Siddhartha",
 course: "MCA",
 year: 2016,
 status: "Working",
 tech: "Angular"
 },
 {
 name: "Prachi",
 course: "MCA",
 year: 2016,
 status: "Searching",
 tech: "Android"
 },
 {
 name: "Charu",
 course: "MTech",
 year: 2018,
 status: "Preparing",
 tech: "React"
 },
 {
 name: "Harsh",
 course: "BSc",
 year: 2019,
 status: "Preparing",
 tech: "Angular"
 }
];

tempArray = [];

    team = 'React';
    page = '1';
  courses = '';
  status = '';
  coursesStructure = null;
  statusStructure = null;
  coursesSelected = '';
  currPage = 1;
  maxPage = 10;
  x= 0;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.team = params.get('team');
    });
  this.route.queryParamMap.subscribe(params => {
    this.page = params.get('page');
    this.courses = params.get('courses');
    this.status = params.get('status');
    this.makeStructs();
  });
}


makeStructs() {
  console.log(this.team);


  this.currPage = this.page ? +this.page : 1;
  this.coursesSelected = this.courses ? this.courses : "";
  this.coursesStructure = this.coursesArray.map(pl1 => ({
    name : pl1,
    selected: false
  }));
  let temp1 = this.coursesSelected.split(",");
  for ( let i = 0; i < temp1.length; i++ ) {
let item = this.coursesStructure.find(p1 => p1.name === temp1[i]);
if (item) item.selected = true;
  }
  this.statusStructure = {
    sports: this.statusArray,
    selected: this.status ? this.status : ""
  };
  console.log(this.coursesStructure);
  console.log(this.statusStructure);
}


optChange() {
this.x === 1;
  let temp1 = this.coursesStructure.filter(p1 => p1.selected);
  let temp2 = temp1.map(p1 => p1.name);
  this.coursesSelected = temp2.join(",");
  this.tempArray = this.mainArray.filter( a => a.tech === this.team );
if(temp1.length > 0) {
 this.tempArray = this.tempArray.filter(function(a) {

   for (let i = 0; i < temp1.length; i++) {

if (a.course === temp1[i].name) {
  return a;
}
   }
 });
}
if(this.statusStructure.selected != '') {
 this.tempArray = this.tempArray.filter( a => a.status === this.statusStructure.selected );
}

 console.log(this.tempArray);

  let path = "/team/" + this.team;
  let qparams = {};
  if (this.coursesSelected)
    qparams['courses'] = this.coursesSelected;
    if (this.statusStructure.selected)
    qparams["status"] = this.statusStructure.selected;
    this.router.navigate([path], { queryParams: qparams });
}



gotoPage(x) {
  this.currPage = this.currPage + x;
  let path = "/team/" + this.team;
  this.router.navigate([path], {
    queryParams: {
      page: this.currPage
    },
    queryParamsHandling: "merge"
  });
}*/

RAMArray = ['3GB', '4GB', '6GB',];
ROMArray = ['32GB', '64GB', '128GB',];
priceArray = ['Below 10,000', '10,000 or More', ];

mainArray = [
  {
  name: "Mi A1",
  brand: "Xiaomi",
  RAM: "4GB",
  ROM: "64GB",
  price: 9999
  },
  {
  name: "Realme 2",
  brand: "Realme",
  RAM: "3GB",
  ROM: "32GB",
  price: 7999
  },
  {
  name: "On5",
  brand: "Samsung",
  RAM: "3GB",
  ROM: "64GB",
  price: 8499
  },
  {
  name: "F5",
  brand: "Oppo",
  RAM: "6GB",
  ROM: "128GB",
  price: 16999
  },
  {
  name: "Mi A3",
  brand: "Xiaomi",
  RAM: "6GB",
  ROM: "128GB",
  price: 18999
  },
  {
  name: "Realme 3",
  brand: "Realme",
  RAM: "4GB",
  ROM: "64GB",
  price: 9999
  },
  {
  name: "On7",
  brand: "Samsung",
  RAM: "4GB",
  ROM: "64GB",
  price: 11999
  },
  {
  name: "F7",
  brand: "Oppo",
  RAM: "6GB",
  ROM: "128GB",
  price: 18199
  },
  {
  name: "RedMi 5",
  brand: "Xiaomi",
  RAM: "3GB",
  ROM: "32GB",
  price: 7499
  },
  {
  name: "Realme Pro",
  brand: "Realme",
  RAM: "4GB",
  ROM: "64GB",
  price: 10299
  },
  {
  name: "Galaxy 9",
  brand: "Samsung",
  RAM: "6GB",
  ROM: "128GB",
  price: 35999
  },
  {
  name: "F3 Basic",
  brand: "Oppo",
  RAM: "3GB",
  ROM: "32GB",
  price: 8599
  }
 ];

tempArray = [];

    team = 'AllBrands';

  rams = '';
  roms = '';
  price = '';
  ramsStructure = null;
  romsStructure = null;
  priceStructure = null;
  ramsSelected = '';
  romsSelected = '';


  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.team = params.get('team');
    });
  this.route.queryParamMap.subscribe(params => {

    this.rams = params.get('courses');
    this.roms = params.get('courses');
    this.price = params.get('status');
    this.makeStructs();
  });
}


makeStructs() {
  console.log(this.team);
  if(this.team === 'AllBrands') {
    this.tempArray = [...this.mainArray];
  }

  this.ramsSelected = this.rams ? this.rams : "";
  this.ramsStructure = this.RAMArray.map(pl1 => ({
    name : pl1,
    selected: false
  }));
  this.romsSelected = this.roms ? this.roms : "";
  this.romsStructure = this.ROMArray.map(pl1 => ({
    name : pl1,
    selected: false
  }));
  let temp1 = this.ramsSelected.split(",");
  let temp2 = this.romsSelected.split(",");
  for ( let i = 0; i < temp1.length; i++ ) {
let item = this.ramsStructure.find(p1 => p1.name === temp1[i]);
if (item) item.selected = true;
  }
  for ( let i = 0; i < temp2.length; i++ ) {
    let item = this.romsStructure.find(p1 => p1.name === temp2[i]);
    if (item) item.selected = true;
      }
  this.priceStructure = {
    sports: this.priceArray,
    selected: this.price ? this.price : ""
  };
  console.log(this.ramsStructure);
  console.log(this.priceStructure);
}


optChange() {

  let temp1 = this.ramsStructure.filter(p1 => p1.selected);
  let temp2 = this.romsStructure.filter(p1 => p1.selected);

  if (this.team === 'AllBrands' ) {
    this.tempArray = [...this.mainArray];
  }
else {
  this.tempArray = this.mainArray.filter( a => a.brand === this.team );
}

if (temp1.length > 0) {

 this.tempArray = this.tempArray.filter(function(a) {

   for (let i = 0; i < temp1.length; i++) {

if (a.RAM === temp1[i].name) {
  console.log(a.RAM);
  return a;
}
   }
 });
}

if(temp2.length > 0) {

  this.tempArray = this.tempArray.filter(function(a) {

    for (let i = 0; i < temp2.length; i++) {

 if (a.ROM === temp2[i].name) {

   return a;
 }
    }
  });
 }

if(this.priceStructure.selected != '') {
  var p = this.priceStructure.selected;
  console.log(p);
  console.log(this.priceStructure.selected);
 this.tempArray = this.tempArray.filter( function (a)  {
   console.log("ferge");
if ( p === 'Below 10,000') {
  console.log("pp");
if( a.price < 10000) {
  console.log(a.price);
  console.log("true1");
return a;
}

}
else{
  if(a.price > 10000) {
    return a;
    }
}
 } );
}

 console.log(this.tempArray);

  let path = "/team/" + this.team;

  let qparams = {};
  if (this.ramsSelected)
    qparams['rams'] = this.ramsSelected;
    if (this.priceStructure.selected)
    qparams["price"] = this.priceStructure.selected;
    this.router.navigate([path], { queryParams: qparams });

}
}
