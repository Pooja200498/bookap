
import { Component, OnInit, Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']

})
export class StudentComponent implements OnInit {
  @Output() childEvent = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  addToFaculty(value: string) {
    console.log(value);
    this.childEvent.emit(value);
  }

}
