import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.css']
})
export class MobileComponent implements OnInit {
@Input() amobile;
@Output() selMob = new EventEmitter();
qty = 1;
  constructor() { }

  ngOnInit() {
    this.amobile.quantity = this.qty;
  }
addToCart() {
  this.selMob.emit( this.amobile);
}

}
