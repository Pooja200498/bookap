import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-new-match',
  templateUrl: './new-match.component.html',
  styleUrls: ['./new-match.component.css']
})
export class NewMatchComponent implements OnInit {
@Input() allteams;
@Output() changeProperties = new EventEmitter();
@Output() changedData = new EventEmitter();
@Output() changedNewData = new EventEmitter();

selTeam1 = 'Choose Team 1';
selTeam2 = 'Choose Team 2';
team1 = null;
team2 = null;
Property4 = false;
condition = true;

selectedTeam1(i) {
this.team1 = this.allteams[i];
this.selTeam1 = 'Team 1 : ' + this.team1;
}
selectedTeam2(j) {
  this.team2 = this.allteams[j];
  this.selTeam2 = 'Team 2 : ' + this.team2;
  }

  startMatch() {
if (this.team1 === null) {
  alert('Select Team 1');

}
else if (this.team2 === null) {
  alert('Select Team 2');

}
else if (this.team1 === this.team2) {
  alert('Select different teams');

}
else{
this.Property4 = true;
this.condition = false;
this.changeProperties.emit();


}
  }

  changed(data) {
console.log(data);
this.changedData.emit(data);
this.Property4 = false;
this.condition = true;
  }

  changedArray(s) {
this.changedNewData.emit(s);
  }

  constructor() { }

  ngOnInit() {

  }



}
