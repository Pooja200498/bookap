import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cart-nav-bar',
  templateUrl: './cart-nav-bar.component.html',
  styleUrls: ['./cart-nav-bar.component.css']
})
export class CartNavBarComponent implements OnInit {
@Input() noOfMobilesInCart;
  constructor() { }

  ngOnInit() {
  }

}
