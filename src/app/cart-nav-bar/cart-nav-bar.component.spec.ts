import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartNavBarComponent } from './cart-nav-bar.component';

describe('CartNavBarComponent', () => {
  let component: CartNavBarComponent;
  let fixture: ComponentFixture<CartNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
