import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-pic-viewer',
  templateUrl: './pic-viewer.component.html',
  styleUrls: ['./pic-viewer.component.css']
})
export class PicViewerComponent implements OnInit {
@Input() pic ;
@Input() num ;
@Output() public favPic = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  addtoFavourite(){
this.favPic.emit();
  }
}
