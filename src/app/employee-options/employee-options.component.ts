import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employee-options',
  templateUrl: './employee-options.component.html',
  styleUrls: ['./employee-options.component.css']
})
export class EmployeeOptionsComponent implements OnInit {
 @Input() departmentsCB;
@Input() designationRadio;
@Output() optSel = new EventEmitter();
  constructor() { }

  ngOnInit() {
 
  }
  emitChange() {
    this.optSel.emit();
  }

}
