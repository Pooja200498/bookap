import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeOptionsComponent } from './employee-options.component';

describe('EmployeeOptionsComponent', () => {
  let component: EmployeeOptionsComponent;
  let fixture: ComponentFixture<EmployeeOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
