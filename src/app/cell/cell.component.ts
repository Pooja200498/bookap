import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {
@Input() cellno;
@Input() index;
@Output() enterValue = new EventEmitter();
  constructor() { }

  displayValue(n) {
    if (this.cellno.value === -1) {
this.enterValue.emit(n);
    }
  }

  ngOnInit() {
  }

}
