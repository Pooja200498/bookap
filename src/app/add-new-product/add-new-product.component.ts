import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-new-product',
  templateUrl: './add-new-product.component.html',
  styleUrls: ['./add-new-product.component.css']
})
export class AddNewProductComponent implements OnInit {
@Output() updatedData = new EventEmitter();
@Output() updatedData1 = new EventEmitter();

brands =["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys", "P&G", "Colgate", "Parachute",
"Gillete", "Dove", "Levis", "Van Heusen", "Manyavaar", "Zara"];

Property1 = false;
Property2 = false;
code = '';
price;
brand = '';
specialOffer:boolean = false;
limitedStock:boolean = false;
category: string = '';
stock;
offer;


addProduct() {
  this.Property1 = false;
  this.Property2 = false;

  if(this.code === '') {
    this.Property1 = true;
  }
if(this.price === undefined) {
    this.Property2 = true;
  }
  else if (this.category === '') {
    alert('Category is required');
  }
  else if (this.brand === '') {
    console.log(this.specialOffer);
    console.log(this.limitedStock);
    alert('Brand is required');
  }
  else{
this.offer = this.specialOffer;
this.stock = this.limitedStock;

  var data = {code: this.code, price: this.price, brand: this.brand , category: this.category,
  specialOffer: this.offer, limitedStock: this.stock , quantity: 0};
  console.log(data);
  console.log(this.specialOffer);
this.updatedData.emit(data);
  }
}

goBackToHome() {
  this.updatedData1.emit();
}

  constructor() { }

  ngOnInit() {
  }

}
