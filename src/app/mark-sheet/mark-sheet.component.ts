import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mark-sheet',
  templateUrl: './mark-sheet.component.html',
  styleUrls: ['./mark-sheet.component.css']
})
export class MarkSheetComponent implements OnInit {
@Input() showMarkSheet;
@Input() checkIndex;
@Output() changeAnswer = new EventEmitter();
@Output() submitAss = new EventEmitter();
Property1 = true;
Property2 = false;

  constructor() { }

  editAnswer(j) {
this.changeAnswer.emit(j);
  }

  submitAssignment() {
this.submitAss.emit();
  }

  ngOnInit() {
    console.log(this.showMarkSheet);
    if (this.checkIndex > -1) {
this.Property1 = false;
this.Property2 = true;
    }
  }

}
