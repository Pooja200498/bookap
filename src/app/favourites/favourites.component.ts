import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {
@Input() picfav;
@Output() deletedPic = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
remove() {
  this.deletedPic.emit(this.picfav);
}
}
