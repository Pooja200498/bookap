import { Competitor } from './../match';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.css']
})
export class CompetitionComponent implements OnInit {
@Input() comp: Competitor;
@Output() compChanged = new EventEmitter<Competitor>();
  constructor() { }

  ngOnInit() {
  }
add(x: number) {
  this.comp.score = this.comp.score + x ;
  this.compChanged.emit(this.comp);
}
}
