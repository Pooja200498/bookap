import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NetService } from './../net.service';
import { bloomAdd } from '@angular/core/src/render3/di';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  /*url1 = 'http://localhost:2410/booksapp/books';
  url2 = '';
  strData;
  data1: any;
  data: any;
  genre = '';
  prevGenre = '';
  newArrival = null;
  pageNumber = 1;
  numberOfPages = 0;
  nextPageNumber = 1;
  totalItemCount = 0;
  fromPage = 1;
  toPage = 10;
  arrValue = 0;
  PageValue = 0;
x = 0;
arr = [];
bestSellerOptions = {};
languageOptions = {};
bestSellerArray = [];
languageArray = [];
selBestSeller = '';
selLanguage = '';
a = 0;
 path;
 prevbestSeller = '';
 prevlanguage = '' ;


  constructor(private  netService: NetService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
      this.pageChange();
      this.route.paramMap.subscribe(params => {
        this.genre = params.get('genre');
        console.log(this.genre);
        if (this.prevGenre != this.genre) {
          this.pageChange();
        }
        this.prevGenre = this.genre;
        this.emitChange();
      });

      this.route.queryParamMap.subscribe(params => {
        this.newArrival = params.get('newarrival');
      // this.nextPageNumber = +(params.get('page'));
        console.log(this.genre);

  if (this.nextPageNumber === 1) {
  this.pageChange();
  }
     this.emitChange();
     });

    }
pageChange() {
     this.nextPageNumber = 1;
  }
 previous(n) {
     this.x = n;
     this.changePage();
   }

   next(n) {
  this.x = n;
   this.changePage();
   }

   changePage() {
     var myparams = {};
     this.pageNumber = this.data1.pageInfo.pageNumber;
     this.nextPageNumber = this.data1.pageInfo.pageNumber + this.x;
     this.numberOfPages = this.data1.pageInfo.numberOfPages;

     if (this.genre === null && this.newArrival === null) {
     this.path = '/books';

     myparams['page'] = this.nextPageNumber;
     }
     else if (this.genre != null && this.newArrival === null) {
      this.path = '/books/' + this.genre;
     myparams['page'] = this.nextPageNumber;
     }
     else{
       this.path = '/books';
      myparams['newarrival'] = 'Yes';
     myparams['page'] = this.nextPageNumber;

     }
    this.router.navigate([this.path], {queryParams: myparams} );
     this.emitChange();
   }


    leftPanelData() {
      this.a = 1;
      for (let i = 0; i < this.bestSellerArray.length; i++ ) {
        if(this.bestSellerArray[i].refineValue === this.selBestSeller) {
        this.bestSellerArray[i]['isSelected'] = true;
        }
        else {this.bestSellerArray[i]['isSelected'] = false;}
      }
      for (let i = 0; i < this.languageArray.length; i++ ) {
        if(this.languageArray[i].refineValue === this.selLanguage) {
          this.languageArray[i]['isSelected'] = true;
          }
          else {this.languageArray[i]['isSelected'] = false;}
        }

    this.bestSellerOptions['options'] = this.bestSellerArray;
    this.bestSellerOptions['display'] = 'BestSeller';
    this.bestSellerOptions['selected'] = '';

   this.languageOptions['options'] = this.languageArray;
    this.languageOptions['display'] = 'Language';
    this.languageOptions['selected'] = '';

    }

emitChange() {
  this.arr = [];
  this.selBestSeller = '';
  this.selLanguage = '';
  if (this.genre != null) {
    this.url2 = this.url1 + '/' + this.genre;
      }
      if (this.newArrival != null) {
        this.url2 = this.url1 + '?newarrival=' + this.newArrival;
      }
      if (this.genre === null && this.newArrival === null) {
        this.url2 = this.url1;
      }

  if (this.a > 0) {
  for (let i = 0; i < this.bestSellerOptions.options.length; i++ ) {
    if (this.bestSellerOptions.options[i].isSelected === true) {
  this.bestSellerOptions.selected = this.bestSellerOptions.options[i].refineValue;
  this.selBestSeller = this.bestSellerOptions.options[i].refineValue;
    }
  }
  for (let i = 0; i < this.languageOptions.options.length; i++ ) {

    if (this.languageOptions.options[i].isSelected === true) {
  this.languageOptions.selected = this.languageOptions.options[i].refineValue;
  this.selLanguage = this.languageOptions.options[i].refineValue;
    }
  }
}

if (this.prevbestSeller !== this.selBestSeller) {
  this.nextPageNumber = 1;
    }
    if (this.prevlanguage !== this.selLanguage) {
      this.nextPageNumber = 1;
        }
        this.prevlanguage = this.selLanguage;
        this.prevbestSeller = this.selBestSeller;
  if (this.newArrival === null) {
  if(this.selBestSeller === '' && this.selLanguage != '' ) {
    this.url2 = this.url2 + '?language=' + this.selLanguage;

    }
   else if (this.selBestSeller != '' && this.selLanguage === '') {
      this.url2 = this.url2 + '?bestseller=' + this.selBestSeller;
    }
    else if (this.selBestSeller != '' && this.selLanguage != '') {
      this.url2 = this.url2 + '?language=' + this.selLanguage + '&bestseller=' + this.selBestSeller;
}
  }
  else {
    if(this.selBestSeller === '' && this.selLanguage != '' ) {
      this.url2 = this.url2 + '&language=' + this.selLanguage;

      }
     else if (this.selBestSeller != '' && this.selLanguage === '') {
        this.url2 = this.url2 + '&bestseller=' + this.selBestSeller;
      }
      else if (this.selBestSeller != '' && this.selLanguage != '') {
        this.url2 = this.url2 + '&language=' + this.selLanguage + '&bestseller=' + this.selBestSeller;
  }
  }


  if (this.newArrival === null && this.selBestSeller === '' && this.selLanguage === '' ) {
    this.url2 = this.url2 + '?page=' + this.nextPageNumber;
  }
  else {
  this.url2 = this.url2 + '&page=' + this.nextPageNumber;
  }

this.netService.getData(this.url2)
.subscribe(resp => {
this.strData = JSON.stringify(resp);
this.data1 = resp;

this.data = this.data1.data;
this.numberOfPages = this.data1.pageInfo.numberOfPages;
this.totalItemCount = this.data1.pageInfo.totalItemCount;
this.fromPage = (this.nextPageNumber-1) * 10 + 1;
if (this.nextPageNumber < this.numberOfPages) {
this.toPage = (this.nextPageNumber-1) * 10 + this.data1.pageInfo.numOfItems;
}
else {
  this.toPage = this.totalItemCount  ;
}

this.bestSellerArray = this.data1.refineOptions.bestseller;
this.languageArray = this.data1.refineOptions.language;
this.leftPanelData();
});


if (this.newArrival != null) {
  this.arr.push({newarrival : this.newArrival});
}
if (this.selLanguage !== '') {
  this.arr.push({language: this.selLanguage});
}
if (this.selBestSeller !== '') {
  this.arr.push({bestseller: this.selBestSeller});
}

this.arr.push({page: this.nextPageNumber});
console.log(this.arr);
var myparams = this.arr.reduce((arr, item) => Object.assign(arr, item, {}));

this.router.navigate([this.path],
  {queryParams: myparams} );

}*/









 url1 = 'http://localhost:2410/booksapp/books';
  url2 = '';
  strData;
  data1: any;
  data: any;
  genre = '';
  prevGenre = '';
  newArrival = null;
  pageNumber = 1;
  numberOfPages = 0;
  nextPageNumber = 1;
  totalItemCount = 0;
  fromPage = 1;
  toPage = 10;
  arrValue = 0;
  PageValue = 0;
x = 0;
arr = [];
bestSellerOptions = {};
languageOptions = {};
bestSellerArray = [];
languageArray = [];
selBestSeller = '';
selLanguage = [];
a = 0;
 path = '/books';
 prevbestSeller = '';
 prevlanguage = '' ;
 prevArrival = '';


  constructor(private  netService: NetService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
      this.nextPageNumber = 1;
      this.route.paramMap.subscribe(params => {
        this.genre = params.get('genre');

        if (this.prevGenre != this.genre) {
          this.nextPageNumber = 1;
        }
        this.prevGenre = this.genre;
        this.emitChange();
      });

      this.route.queryParamMap.subscribe(params => {
        this.newArrival = params.get('newarrival');
        if (this.prevArrival !== this.newArrival) {
          this.nextPageNumber = 1;
        }
        this.prevArrival = this.newArrival;


  if (this.nextPageNumber === 1) {
    this.nextPageNumber = 1;
  }
     this.emitChange();
     });

    }

 previous(n) {
     this.x = n;
     this.changePage();
   }

   next(n) {
  this.x = n;
   this.changePage();
   }

   changePage() {
     var myparams = {};
     this.pageNumber = this.data1.pageInfo.pageNumber;
     this.nextPageNumber = this.data1.pageInfo.pageNumber + this.x;
     this.numberOfPages = this.data1.pageInfo.numberOfPages;
     this.emitChange();
   }


    leftPanelData() {
      this.a = 1;
      for (let i = 0; i < this.bestSellerArray.length; i++ ) {
        if (this.bestSellerArray[i].refineValue === this.selBestSeller) {
        this.bestSellerArray[i]['isSelected'] = true;
        }
        else {
          this.bestSellerArray[i]['isSelected'] = false;
      }
      }
      for (let i = 0; i < this.languageArray.length; i++ ) {
        this.languageArray[i]['isSelected'] = false;
      }

      for (let i = 0; i < this.languageArray.length; i++ ) {
        for (let j = 0; j < this.selLanguage.length ; j++) {
        if (this.languageArray[i].refineValue === this.selLanguage[j]) {
          this.languageArray[i]['isSelected'] = true;
          }
        }
      }

    this.bestSellerOptions['options'] = this.bestSellerArray;
    this.bestSellerOptions['display'] = 'BestSeller';
    this.bestSellerOptions['selected'] = '';

   this.languageOptions['options'] = this.languageArray;
    this.languageOptions['display'] = 'Language';
    this.languageOptions['selected'] = '';
    }

emitChange() {
  this.arr = [];
  this.selBestSeller = '';
  this.selLanguage = [];

  if (this.genre != null) {
    this.url2 = this.url1 + '/' + this.genre;
      }
      if (this.newArrival != null) {
        this.url2 = this.url1 + '?newarrival=' + this.newArrival;
      }
      if (this.genre === null && this.newArrival === null) {
        this.url2 = this.url1;
      }

  if (this.a > 0) {
  for (let i = 0; i < this.bestSellerOptions.options.length; i++ ) {
    if (this.bestSellerOptions.options[i].isSelected === true) {
  this.bestSellerOptions.selected = this.bestSellerOptions.options[i].refineValue;
  this.selBestSeller = this.bestSellerOptions.options[i].refineValue;
    }
  }
  for (let i = 0; i < this.languageOptions.options.length; i++ ) {

    if (this.languageOptions.options[i].isSelected === true) {
  this.languageOptions.selected = this.languageOptions.options[i].refineValue;
  this.selLanguage.push(this.languageOptions.options[i].refineValue);
    }
  }
}

        this.prevlanguage = this.languageOptions.selected;
        this.prevbestSeller = this.selBestSeller;
        var selLanguages = '';

        for (let i = 0; i < this.selLanguage.length ; i++) {
          if ((this.selLanguage.length - 1) ===  i) {
            selLanguages = selLanguages + this.selLanguage[i];
          }
          else {
          selLanguages = selLanguages + this.selLanguage[i] + ',';
          }
        }


  if (this.newArrival === null) {
  if (this.selBestSeller === '' && this.selLanguage.length > 0 ) {
 this.url2 = this.url2 + '?language=' + selLanguages;
 }
   else if (this.selBestSeller !== '' && this.selLanguage.length < 1) {
      this.url2 = this.url2 + '?bestseller=' + this.selBestSeller;
    }
    else if (this.selBestSeller != '' && this.selLanguage.length > 0) {
      this.url2 = this.url2 + '?language=' + selLanguages + '&bestseller=' + this.selBestSeller;
    }
  }
  else {
    if (this.selBestSeller === '' && this.selLanguage.length > 0 ) {
      this.url2 = this.url2 + '&language=' + selLanguages;
      }
     else if (this.selBestSeller !== '' && this.selLanguage.length < 1 ) {
        this.url2 = this.url2 + '&bestseller=' + this.selBestSeller;
      }
      else if (this.selBestSeller !== '' && this.selLanguage.length > 0 ) {
        this.url2 = this.url2 + '&language=' + selLanguages + '&bestseller=' + this.selBestSeller;
      }
  }

  if (this.newArrival === null && this.selBestSeller === '' && this.selLanguage.length < 1 ) {
    this.url2 = this.url2 + '?page=' + this.nextPageNumber;
  }
  else {
  this.url2 = this.url2 + '&page=' + this.nextPageNumber;
  }

  console.log(this.url2);
this.netService.getData(this.url2)
.subscribe(resp => {
this.strData = JSON.stringify(resp);
this.data1 = resp;

this.data = this.data1.data;
this.numberOfPages = this.data1.pageInfo.numberOfPages;
this.totalItemCount = this.data1.pageInfo.totalItemCount;
this.fromPage = (this.nextPageNumber-1) * 10 + 1;
if (this.nextPageNumber < this.numberOfPages) {
this.toPage = (this.nextPageNumber-1) * 10 + this.data1.pageInfo.numOfItems;
}
else {
  this.toPage = this.totalItemCount  ;
}

this.bestSellerArray = this.data1.refineOptions.bestseller;
this.languageArray = this.data1.refineOptions.language;
this.leftPanelData();
});


if (this.newArrival != null) {
  this.arr.push({newarrival : this.newArrival});
}

if (this.selLanguage.length > 0 ) {
  this.arr.push({language: selLanguages});
}
if (this.selBestSeller !== '') {
  this.arr.push({bestseller: this.selBestSeller});
}

this.arr.push({page: this.nextPageNumber});

  if (this.genre != null && this.newArrival === null) {
   this.path = '/books/' + this.genre;
  }


var myparams = this.arr.reduce((arr, item) => Object.assign(arr, item, {}));

this.router.navigate([this.path],
  {queryParams: myparams} );

}

refine() {
  this.nextPageNumber = 1;
  this.emitChange();
}













 /*url1 = 'http://localhost:2410/booksapp/books';
  url2 = '';
  strData;
  data1: any;
  data: any;
  genre = '';
  prevGenre = '';
  newArrival = null;
  pageNumber = 1;
  numberOfPages = 0;
  nextPageNumber = 1;
  totalItemCount = 0;
  fromPage = 1;
  toPage = 10;
  arrValue = 0;
  PageValue = 0;
x = 0;
arr = [];
bestSellerOptions = {};
languageOptions = {};
bestSellerArray = [];
languageArray = [];
selBestSeller = '';
selLanguage = '';
a = 0;
 path;
 prevbestSeller = '';
 prevlanguage = '' ;


  constructor(private  netService: NetService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
      this.pageChange();
      this.route.paramMap.subscribe(params => {
        this.genre = params.get('genre');
        console.log(this.genre);
        if (this.prevGenre != this.genre) {
          this.pageChange();
        }
        this.prevGenre = this.genre;
        this.emitChange();
      });

      this.route.queryParamMap.subscribe(params => {
        this.newArrival = params.get('newarrival');
       this.nextPageNumber = +(params.get('page'));
        console.log(this.genre);

  if (this.nextPageNumber === 1) {
  this.pageChange();
  }
     this.emitChange();
     });

    }
pageChange() {
     this.nextPageNumber = 1;
  }
 previous(n) {
     this.x = n;
     this.changePage();
   }

   next(n) {
  this.x = n;
   this.changePage();
   }

   changePage() {
     var myparams = {};
     this.pageNumber = this.data1.pageInfo.pageNumber;
     this.nextPageNumber = this.data1.pageInfo.pageNumber + this.x;
     this.numberOfPages = this.data1.pageInfo.numberOfPages;

     if (this.genre === null && this.newArrival === null) {
     this.path = '/books';

     myparams['page'] = this.nextPageNumber;
     }
     else if (this.genre != null && this.newArrival === null) {
      this.path = '/books/' + this.genre;
     myparams['page'] = this.nextPageNumber;
     }
     else{
       this.path = '/books';
      myparams['newarrival'] = 'Yes';
     myparams['page'] = this.nextPageNumber;

     }
    this.router.navigate([this.path], {queryParams: myparams} );
     this.emitChange();
   }


    leftPanelData() {
      this.a = 1;
      for (let i = 0; i < this.bestSellerArray.length; i++ ) {
        if(this.bestSellerArray[i].refineValue === this.selBestSeller) {
        this.bestSellerArray[i]['isSelected'] = true;
        }
        else {this.bestSellerArray[i]['isSelected'] = false;}
      }
      for (let i = 0; i < this.languageArray.length; i++ ) {
        if(this.languageArray[i].refineValue === this.selLanguage) {
          this.languageArray[i]['isSelected'] = true;
          }
          else {this.languageArray[i]['isSelected'] = false;}
        }

    this.bestSellerOptions['options'] = this.bestSellerArray;
    this.bestSellerOptions['display'] = 'BestSeller';
    this.bestSellerOptions['selected'] = '';

   this.languageOptions['options'] = this.languageArray;
    this.languageOptions['display'] = 'Language';
    this.languageOptions['selected'] = '';

    }

emitChange() {
  this.arr = [];
  this.selBestSeller = '';
  this.selLanguage = '';
  if (this.genre != null) {
    this.url2 = this.url1 + '/' + this.genre;
      }
      if (this.newArrival != null) {
        this.url2 = this.url1 + '?newarrival=' + this.newArrival;
      }
      if (this.genre === null && this.newArrival === null) {
        this.url2 = this.url1;
      }

  if (this.a > 0) {
  for (let i = 0; i < this.bestSellerOptions.options.length; i++ ) {
    if (this.bestSellerOptions.options[i].isSelected === true) {
  this.bestSellerOptions.selected = this.bestSellerOptions.options[i].refineValue;
  this.selBestSeller = this.bestSellerOptions.options[i].refineValue;
    }
  }
  for (let i = 0; i < this.languageOptions.options.length; i++ ) {

    if (this.languageOptions.options[i].isSelected === true) {
  this.languageOptions.selected = this.languageOptions.options[i].refineValue;
  this.selLanguage = this.languageOptions.options[i].refineValue;
    }
  }
}

if (this.prevbestSeller !== this.selBestSeller) {
  this.nextPageNumber = 1;
    }
    if (this.prevlanguage !== this.selLanguage) {
      this.nextPageNumber = 1;
        }
        this.prevlanguage = this.selLanguage;
        this.prevbestSeller = this.selBestSeller;
  if (this.newArrival === null) {
  if(this.selBestSeller === '' && this.selLanguage != '' ) {
    this.url2 = this.url2 + '?language=' + this.selLanguage;

    }
   else if (this.selBestSeller != '' && this.selLanguage === '') {
      this.url2 = this.url2 + '?bestseller=' + this.selBestSeller;
    }
    else if (this.selBestSeller != '' && this.selLanguage != '') {
      this.url2 = this.url2 + '?language=' + this.selLanguage + '&bestseller=' + this.selBestSeller;
}
  }
  else {
    if(this.selBestSeller === '' && this.selLanguage != '' ) {
      this.url2 = this.url2 + '&language=' + this.selLanguage;

      }
     else if (this.selBestSeller != '' && this.selLanguage === '') {
        this.url2 = this.url2 + '&bestseller=' + this.selBestSeller;
      }
      else if (this.selBestSeller != '' && this.selLanguage != '') {
        this.url2 = this.url2 + '&language=' + this.selLanguage + '&bestseller=' + this.selBestSeller;
  }
  }


  if (this.newArrival === null && this.selBestSeller === '' && this.selLanguage === '' ) {
    this.url2 = this.url2 + '?page=' + this.nextPageNumber;
  }
  else {
  this.url2 = this.url2 + '&page=' + this.nextPageNumber;
  }

this.netService.getData(this.url2)
.subscribe(resp => {
this.strData = JSON.stringify(resp);
this.data1 = resp;

this.data = this.data1.data;
this.numberOfPages = this.data1.pageInfo.numberOfPages;
this.totalItemCount = this.data1.pageInfo.totalItemCount;
this.fromPage = (this.nextPageNumber-1) * 10 + 1;
if (this.nextPageNumber < this.numberOfPages) {
this.toPage = (this.nextPageNumber-1) * 10 + this.data1.pageInfo.numOfItems;
}
else {
  this.toPage = this.totalItemCount  ;
}

this.bestSellerArray = this.data1.refineOptions.bestseller;
this.languageArray = this.data1.refineOptions.language;
this.leftPanelData();
});


if (this.newArrival != null) {
  this.arr.push({newarrival : this.newArrival});
}
if (this.selLanguage !== '') {
  this.arr.push({language: this.selLanguage});
}
if (this.selBestSeller !== '') {
  this.arr.push({bestseller: this.selBestSeller});
}

this.arr.push({page: this.nextPageNumber});
console.log(this.arr);
var myparams = this.arr.reduce((arr, item) => Object.assign(arr, item, {}));
if (this.arr.length > 1) {
this.router.navigate([this.path],
  {queryParams: myparams} );
}
}*/










 /*url1 = 'http://localhost:2410/booksapp/books';
  url2 = '';
  strData;
  data1: any;
  data: any;
  genre = '';
  prevGenre = '';
  newArrival = null;
  pageNumber = 1;
  numberOfPages = 0;
  nextPageNumber = 1;
  totalItemCount = 0;
  fromPage = 1;
  toPage = 10;
  arrValue = 0;
  PageValue = 0;
x = 0;
arr = [];
bestSellerOptions = {};
languageOptions = {};
bestSellerArray = [];
languageArray = [];
selBestSeller = '';
selLanguage = [];
a = 0;
 path = '/books';
 prevbestSeller = '';
 prevlanguage = '' ;


  constructor(private  netService: NetService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
      this.pageChange();
      this.route.paramMap.subscribe(params => {
        this.genre = params.get('genre');
        if (this.prevGenre != this.genre) {
          this.pageChange();
        }
        this.prevGenre = this.genre;
        this.emitChange();
      });

      this.route.queryParamMap.subscribe(params => {
        this.newArrival = params.get('newarrival');
        this.nextPageNumber = +(params.get('page'));

  if (this.nextPageNumber === 1) {
  this.pageChange();
  }
     this.emitChange();
     });

    }
pageChange() {
     this.nextPageNumber = 1;
  }
 previous(n) {
     this.x = n;
     this.changePage();
   }

   next(n) {
    console.log(this.nextPageNumber);
  this.x = n;
   this.changePage();
   }

   changePage() {
     var myparams = {};
     this.pageNumber = this.data1.pageInfo.pageNumber;
     this.nextPageNumber = this.data1.pageInfo.pageNumber + this.x;
     this.numberOfPages = this.data1.pageInfo.numberOfPages;
     console.log(this.nextPageNumber);

     if (this.genre === null && this.newArrival === null) {
      this.path = '/books';

      myparams['page'] = this.nextPageNumber;
      }
      else if (this.genre != null && this.newArrival === null) {
       this.path = '/books/' + this.genre;
      myparams['page'] = this.nextPageNumber;
      }
      else{
        this.path = '/books';
       myparams['newarrival'] = 'Yes';
      myparams['page'] = this.nextPageNumber;

      }
      this.router.navigate([this.path], {queryParams: myparams} );
     this.emitChange();
   }


    leftPanelData() {
      this.a = 1;
      for (let i = 0; i < this.bestSellerArray.length; i++ ) {
        if (this.bestSellerArray[i].refineValue === this.selBestSeller) {
        this.bestSellerArray[i]['isSelected'] = true;
        }
        else {
          this.bestSellerArray[i]['isSelected'] = false;
      }
      }
      for (let i = 0; i < this.languageArray.length; i++ ) {
        this.languageArray[i]['isSelected'] = false;
      }

      for (let i = 0; i < this.languageArray.length; i++ ) {
        for (let j = 0; j < this.selLanguage.length ; j++) {
        if (this.languageArray[i].refineValue === this.selLanguage[j]) {
          this.languageArray[i]['isSelected'] = true;
          }
        }
      }

    this.bestSellerOptions['options'] = this.bestSellerArray;
    this.bestSellerOptions['display'] = 'BestSeller';
    this.bestSellerOptions['selected'] = '';

   this.languageOptions['options'] = this.languageArray;
    this.languageOptions['display'] = 'Language';
    this.languageOptions['selected'] = '';

    }

emitChange() {
  this.arr = [];
  this.selBestSeller = '';
  this.selLanguage = [];
  if (this.genre != null) {
    this.url2 = this.url1 + '/' + this.genre;
      }
      if (this.newArrival != null) {
        this.url2 = this.url1 + '?newarrival=' + this.newArrival;
      }
      if (this.genre === null && this.newArrival === null) {
        this.url2 = this.url1;
      }

  if (this.a > 0) {
  for (let i = 0; i < this.bestSellerOptions.options.length; i++ ) {
    if (this.bestSellerOptions.options[i].isSelected === true) {
  this.bestSellerOptions.selected = this.bestSellerOptions.options[i].refineValue;
  this.selBestSeller = this.bestSellerOptions.options[i].refineValue;
    }
  }
  for (let i = 0; i < this.languageOptions.options.length; i++ ) {

    if (this.languageOptions.options[i].isSelected === true) {
  this.languageOptions.selected = this.languageOptions.options[i].refineValue;
  this.selLanguage.push(this.languageOptions.options[i].refineValue);
    }
  }
}
console.log(this.selLanguage);
if (this.prevbestSeller !== this.selBestSeller) {
  this.nextPageNumber = 1;
    }
    if (this.prevlanguage !== this.languageOptions.selected) {
      this.nextPageNumber = 1;
        }
        this.prevlanguage = this.languageOptions.selected;
        this.prevbestSeller = this.selBestSeller;

        var selLanguages = '';
        console.log(this.selLanguage);
        for (let i = 0; i < this.selLanguage.length ; i++) {
          if ((this.selLanguage.length - 1) ===  i) {
            selLanguages = selLanguages + this.selLanguage[i];
            console.log(selLanguages);
          }
          else {
          selLanguages = selLanguages + this.selLanguage[i] + ',';
          console.log(selLanguages);
          }
        }


  if (this.newArrival === null) {
  if (this.selBestSeller === '' && this.selLanguage.length > 0 ) {
 this.url2 = this.url2 + '?language=' + selLanguages;
 }
   else if (this.selBestSeller !== '' && this.selLanguage.length < 1) {
      this.url2 = this.url2 + '?bestseller=' + this.selBestSeller;
    }
    else if (this.selBestSeller != '' && this.selLanguage.length > 0) {
      this.url2 = this.url2 + '?language=' + selLanguages + '&bestseller=' + this.selBestSeller;
    }
  }
  else {
    if (this.selBestSeller === '' && this.selLanguage.length > 0 ) {
      this.url2 = this.url2 + '&language=' + selLanguages;
      }
     else if (this.selBestSeller !== '' && this.selLanguage.length < 1 ) {
        this.url2 = this.url2 + '&bestseller=' + this.selBestSeller;
      }
      else if (this.selBestSeller !== '' && this.selLanguage.length > 0 ) {
        this.url2 = this.url2 + '&language=' + selLanguages + '&bestseller=' + this.selBestSeller;
      }
  }

  if (this.newArrival === null && this.selBestSeller === '' && this.selLanguage.length < 1 ) {
    this.url2 = this.url2 + '?page=' + this.nextPageNumber;
  }
  else {
  this.url2 = this.url2 + '&page=' + this.nextPageNumber;
  }

  console.log(this.url2);
this.netService.getData(this.url2)
.subscribe(resp => {
this.strData = JSON.stringify(resp);
this.data1 = resp;

this.data = this.data1.data;
this.numberOfPages = this.data1.pageInfo.numberOfPages;
this.totalItemCount = this.data1.pageInfo.totalItemCount;
this.fromPage = (this.nextPageNumber-1) * 10 + 1;
if (this.nextPageNumber < this.numberOfPages) {
this.toPage = (this.nextPageNumber-1) * 10 + this.data1.pageInfo.numOfItems;
}
else {
  this.toPage = this.totalItemCount  ;
}

this.bestSellerArray = this.data1.refineOptions.bestseller;
this.languageArray = this.data1.refineOptions.language;
this.leftPanelData();
});



if (this.genre != null) {
  this.arr.push({genre: this.genre});
}
if (this.newArrival != null) {
  this.arr.push({newarrival : this.newArrival});
}
if (this.selLanguage.length > 0) {
  this.arr.push({language: selLanguages});
}
if (this.selBestSeller !== '') {
  this.arr.push({bestseller: this.selBestSeller});
}
this.arr.push({page: this.nextPageNumber});
console.log(this.path);
//if (this.arr.length > 1) {
var myparams = this.arr.reduce((arr, item) => Object.assign(arr, item, {}));
console.log(this.arr);
this.router.navigate([this.path],
  {queryParams: myparams} );
//}
/*else {

    if(this.a > 0) {
    console.log(this.path);
    console.log(this.nextPageNumber);
   this.router.navigate([this.path], {queryParams: {'page': this.nextPageNumber}} );
    }
}
}*/






























/*url1 = 'http://localhost:2410/booksapp/books';
url2 = '';
strData;
data1: any;
data: any;
genre = '';
prevGenre = '';
newArrival = null;
Property1 = false;
Property2 = true;
pageNumber = 1;
numberOfPages = 0;
nextPageNumber = 1;
totalItemCount = 0;
fromPage = 1;
toPage = 10;
x;
bestSellerOptions = {};
languageOptions = {};
bestSellerArray = [];
languageArray = [];
selBestSeller = '';
selLanguage = '';
arr = [];


constructor(private  netService: NetService,private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.pageChange();
    this.route.paramMap.subscribe(params => {
      this.genre = params.get('genre');

      this.arr.push(this.genre);

      if (this.prevGenre != this.genre) {
        this.pageChange();
      }
      this.prevGenre = this.genre;
      this.getNewData();
    });

this.route.queryParamMap.subscribe(params => {
      this.newArrival = params.get('newarrival');
      this.nextPageNumber = +(params.get('page'));
      if (this.newArrival != '') {
        this.arr.push(this.newArrival);
      }

        this.arr.push(this.nextPageNumber);

      console.log(this.newArrival);
      console.log(this.nextPageNumber);
if (this.nextPageNumber === 1) {
this.pageChange();
}
      this.getNewData();
      this.emitChange();

    });

  }


pageChange() {
   this.nextPageNumber = 1;
    this.Property2 = true;
    this.Property1 = false;
}

previous(n) {
 this.Property2 = true;
  this.x = n;
  this.changePage();
if (this.pageNumber === 1) {
    this.Property1 = false;
  }
}

next(n) {
this.Property1 = true;
this.x = n;
this.changePage();
if (this.numberOfPages === this.pageNumber + 1) {
  this.Property2 = false;
}
}

changePage() {
  let path;
  var myparams = {};
  this.pageNumber = this.data1.pageInfo.pageNumber;
  this.nextPageNumber = this.data1.pageInfo.pageNumber + this.x;
  this.numberOfPages = this.data1.pageInfo.numberOfPages;

  if (this.genre === null && this.newArrival === null) {
  path = '/books';

  myparams['page'] = this.nextPageNumber;
  }
  else if (this.genre != null && this.newArrival === null) {
   path = '/books/' + this.genre;
  myparams['page'] = this.nextPageNumber;
  }
  else{
    path = '/books';
   myparams['newarrival'] = 'Yes';
  myparams['page'] = this.nextPageNumber;

  }
 this.router.navigate([path], {queryParams: myparams} );
  this.getNewData();
}


getNewData() {
  if (this.genre != null) {
    this.url2 = this.url1 + '/' + this.genre;

      }
      if (this.newArrival != null) {
        this.url2 = this.url1 + '?newarrival=' + this.newArrival;
      }
      if (this.genre === null && this.newArrival === null) {
        this.url2 = this.url1;
      }
console.log(this.url2);
 if (this.newArrival === null  ) {
  this.url2 = this.url2 + '?page=' + this.nextPageNumber;
}
else {
  this.url2 = this.url2 + '&page=' + this.nextPageNumber;
}
this.netService.getData(this.url2)
.subscribe(resp => {
this.strData = JSON.stringify(resp);
this.data1 = resp;
console.log(this.data1);
this.data = this.data1.data;
this.bestSellerArray = this.data1.refineOptions.bestseller;
this.languageArray = this.data1.refineOptions.language;

console.log(this.data);
this.leftPanelData();
this.fromPage = (this.nextPageNumber-1) * 10 + 1;
this.toPage = (this.nextPageNumber-1) * 10 + this.data1.pageInfo.numOfItems;
this.totalItemCount = this.data1.pageInfo.totalItemCount;
    })
  }

leftPanelData() {
  for (let i = 0; i < this.bestSellerArray.length; i++ ) {
    this.bestSellerArray[i]['isSelected'] = false;
  }
  for (let i = 0; i < this.languageArray.length; i++ ) {
    this.languageArray[i]['isSelected'] = false;
  }


this.bestSellerOptions['options'] = this.bestSellerArray;
this.bestSellerOptions['display'] = 'BestSeller';
this.bestSellerOptions['selected'] = '';


this.languageOptions['options'] = this.languageArray;
this.languageOptions['display'] = 'Language';
this.languageOptions['selected'] = '';

}


emitChange() {

var count1 = 0;
var count2 = 0;
console.log(this.bestSellerOptions);
for (let i = 0; i < this.bestSellerOptions.options.length; i++ ) {
  if (this.bestSellerOptions.options[i].isSelected === true) {
    count1 ++;
this.bestSellerOptions.selected = this.bestSellerOptions.options[i].refineValue;
this.selBestSeller = this.bestSellerOptions.options[i].refineValue;
  }
}
for (let i = 0; i < this.languageOptions.options.length; i++ ) {
  count2++;
  if (this.languageOptions.options[i].isSelected === true) {
this.languageOptions.selected = this.languageOptions.options[i].refineValue;
this.selLanguage = this.languageOptions.options[i].refineValue;
  }
}


var path;
var myparams;
if (this.genre === null && this.newArrival === null) {
  path = '/books';
 }
  else if (this.genre != null && this.newArrival === null) {
   path = '/books/' + this.genre;
 }
  else{
    path = '/books';
  }
if(this.selBestSeller === '' && this.selLanguage != '') {
 this.router.navigate([path], {
  queryParams: {
    language: this.selLanguage
  },
  queryParamsHandling: "merge"
});
this.url2 = this.url2 + '?language=' + this.selLanguage;
}
console.log(this.url2);
this.netService.getData(this.url2)
.subscribe(resp => {
this.strData = JSON.stringify(resp);
this.data1 = resp;
console.log(this.data1);
this.data = this.data1.data;
this.bestSellerArray = this.data1.refineOptions.bestseller;
this.languageArray = this.data1.refineOptions.language;
});
else if (this.selBestSeller != '' && this.selLanguage === '') {
  this.router.navigate([path], {
    queryParams: {
      bestseller : this.selBestSeller
    },
    queryParamsHandling: "merge"
  });
}
else (this.selBestSeller != '' && this.selLanguage != '') {
  this.router.navigate([path], {
    queryParams: {
      language: this.selLanguage ,
      bestseller: this.selBestSeller
    },
    queryParamsHandling: "merge"
  });


  //queryParams = this.arr.reduce((arr, item) => Object.assign(arr, item, {}));
}*/










































  /*  ngOnInit() {

     this.route.paramMap.subscribe(params => {
       this.genre = params.get('genre')
       this.pageNumber = 0;
       this.turn = 0;
       this.Property1 = false;
       console.log("ewewfre");
       this.getData();
     });

 this.route.queryParamMap.subscribe(params => {
       this.newArrival = params.get('newarrival')
       console.log(this.newArrival);
       console.log("nnn");
       this.getData();
      if (this.turn === 1) {
         console.log("again");
         this.next();
       }
       if(this.turn === 2) {
         this.previous();
       }
     });
     this.getData();
     if (this.turn === 1) {
       console.log("again");
       this.next();
     }
     if(this.turn === 2) {
       this.previous();
     }*/



     /*this.route.paramMap.subscribe(params => {
       this.genre = params.get('genre')
       this.pageNumber = 0;
       this.turn = 0;
       this.Property1 = false;

       console.log("params");
       this.getNewData();
     });

 this.route.queryParamMap.subscribe(params => {
       this.newArrival = params.get('newarrival')
       console.log("queryParams");
      this.getNewData();

     });
     this.getNewData();
 }*/

 /*getData() {
   if (this.genre != null) {
 this.url2 = this.url1 + '/' + this.genre;

   }
   if (this.newArrival != null) {
     this.url2 = this.url1 + '?newarrival=' + this.newArrival;
   }
   if (this.genre === null && this.newArrival === null) {
     this.url2 = this.url1;
   }
   console.log(this.url2);
   this.netService.getData(this.url2)
   .subscribe(resp => {
   this.strData = JSON.stringify(resp);
   this.data1 = resp;
   console.log(this.data1);
   this.data = this.data1.data;
   console.log(this.turn);
       })

 }*/

 /*previous() {
   this.turn = 2;
   this.Property2 = true;
   let path;
   console.log(this.data1);
   console.log(this.pageNumber);

   this.pageNumber = this.data1.pageInfo.pageNumber;
   this.nextPageNumber = this.data1.pageInfo.pageNumber - 1;
   this.numberOfPages = this.data1.pageInfo.numberOfPages;

   console.log(this.pageNumber);
   console.log(this.newArrival);
   if (this.genre === null && this.newArrival === null) {
   path = '/books';
   var myparams = {};
   myparams['page'] = this.nextPageNumber;
   }
   else if (this.genre != null && this.newArrival === null) {
    path = '/books/' + this.genre;
   var myparams = {};
   myparams['page'] = this.nextPageNumber;
   }
   else{
     path = '/books';
    var myparams = {};
    myparams['newarrival'] = 'Yes';
   myparams['page'] = this.nextPageNumber;

   }

   this.router.navigate([path], {queryParams: myparams} );
   this.getNewData();
   if (this.pageNumber === 2) {
     this.Property1 = false;
   }
 }

 next() {
 this.turn = 1;
   this.Property1 = true;
 console.log(this.data1);
 let path;
 console.log(this.newArrival);
 console.log(this.pageNumber);
 this.pageNumber = this.data1.pageInfo.pageNumber;
 this.nextPageNumber = this.data1.pageInfo.pageNumber + 1;
 this.numberOfPages = this.data1.pageInfo.numberOfPages;

 if (this.genre === null && this.newArrival === null) {

 path = '/books';
 var myparams = {};
 myparams['page'] = this.nextPageNumber;
 }
 else if (this.genre != null && this.newArrival === null) {


 path = '/books/' + this.genre;
 var myparams = {};
 myparams['page'] = this.nextPageNumber;
 }
 else{

  path = '/books' ;
  var myparams = {};
  myparams['newarrival'] = 'Yes';
 myparams['page'] = this.nextPageNumber;

 }


 console.log(this.data1.pageInfo.pageNumber);
 console.log(this.nextPageNumber);

 this.router.navigate([path], {queryParams: myparams} );
 this.getNewData();
 if(this.numberOfPages === this.pageNumber + 1) {
   this.Property2 = false;
 }
 }


 getNewData() {
   if (this.genre != null) {
     this.url2 = this.url1 + '/' + this.genre;

       }
       if (this.newArrival != null) {
         this.url2 = this.url1 + '?newarrival=' + this.newArrival;
       }
       if (this.genre === null && this.newArrival === null) {
         this.url2 = this.url1;
       }
   console.log(this.newArrival);
  if (this.newArrival === null  ) {
   this.url2 = this.url2 + '?page=' + this.nextPageNumber;
 }
 else {
   this.url2 = this.url2 + '&page=' + this.nextPageNumber;
 }
 console.log(this.url2);
 this.netService.getData(this.url2)
 .subscribe(resp => {
   console.log(resp);
 this.strData = JSON.stringify(resp);
 this.data1 = resp;
 console.log(this.data1);
 this.data = this.data1.data;
 console.log(this.data);
 this.fromPage = this.pageNumber * 10 + 1;
 this.toPage = this.pageNumber * 10 + this.data1.pageInfo.numOfItems;
 this.totalItemCount = this.data1.pageInfo.totalItemCount;
     })
   }*/
}
