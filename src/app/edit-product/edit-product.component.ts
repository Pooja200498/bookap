import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
@Input() products;
@Input() editableData;
@Output() updatedData3 = new EventEmitter();

brands =["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys", "P&G", "Colgate", "Parachute",
"Gillete", "Dove", "Levis", "Van Heusen", "Manyavaar", "Zara"];

code = '';
price;
brand = '';
specialOffer: boolean ;
limitedStock: boolean ;
category: string = '';
stock;
offer;



  constructor() { }

  ngOnInit() {
    console.log(this.editableData);
    this.code = this.products[this.editableData].code;
    this.price = this.products[this.editableData].price;
    this.brand = this.products[this.editableData].brand;
    this.specialOffer = this.products[this.editableData].specialOffer;
    this.limitedStock = this.products[this.editableData].limitedStock;
    this.category = this.products[this.editableData].category;
    console.log(this.brand);
    console.log(this.limitedStock);
   console.log(this.specialOffer);

  }

  goBackToHome() {
    this.updatedData3.emit();
  }

  editProduct() {
     this.products[this.editableData].code = this.code ;
     this.products[this.editableData].price = this.price;
     this.products[this.editableData].brand = this.brand;
    this.products[this.editableData].specialOffer = this.specialOffer;
    this.products[this.editableData].limitedStock = this.limitedStock;
    this.updatedData3.emit();
  }

}
