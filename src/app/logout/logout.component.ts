import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

 /* visitedPages;
  newArray = [];

  constructor(private authService: AuthService,
    private route: ActivatedRoute, private router: Router
    ) { }


  ngOnInit() {
    this.visitedPages = this.authService.visitedPages;
    this.visitedPages = this.visitedPages + 1;
    this.authService.changedData(this.visitedPages);
    this.authService.changedData1('TV');
    this.newArray = this.authService.visitedArray;
  }*/




  laptopArray = [
    {name: 'Acer', model: 'M205', price: '25000' },
    {name: 'Dell', model: 'Vostro6', price: '31000' },
    {name: 'Lenovo', model: 'Yoga 60', price: '27500' },
    {name: 'HP', model: 'H620', price: '29000' },
  ];


  constructor(private authService: AuthService,
    private route: ActivatedRoute, private router: Router
    ) { }

ngOnInit() { }

    add(n) {
      let data =  this.laptopArray[n];
      console.log(data);
      this.authService.addToCart(data);

    }




    /*constructor(private authService: AuthService,
      private route: ActivatedRoute, private router: Router
      ) { }

   ngOnInit() {

    }
    stop() {
      this.authService.logout();
    }*/

}
