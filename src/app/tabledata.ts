export class Table {
  name: string;
  category: string;
  stock: number;
  price: number;
  sales: number;
}
export class Table1 {

}

export let tabledb: Table[] = [
{name: '5Star', category: 'Chocolates', stock: 30, price: 6, sales: 18},
{name: 'Colgate', category: 'Toothpaste', stock: 60, price: 57, sales: 8},
{name: 'Lays-Salted', category: 'Chips', stock: 15, price: 12, sales: 4},
{name: 'Coke', category: 'Beverages', stock: 40, price: 25, sales: 7},
{name: 'Mars', category: 'Chocolates', stock: 30, price: 6, sales: 18},
{name: 'Pepsodent', category: 'Toothpaste', stock: 25, price: 63, sales: 3},
{name: 'Pringles', category: 'Chips', stock: 10, price: 35, sales: 2},
{name: 'Diet Coke', category: 'Beverages', stock: 30, price: 25, sales: 11},
{name: 'Snickers', category: 'Chocolates', stock: 20, price: 10, sales: 8},
{name: 'Sensodyne', category: 'Toothpaste', stock: 20, price: 70, sales: 2},
{name: 'Doritos', category: 'Chips', stock: 16, price: 20, sales: 5},
{name: 'Pepsi', category: 'Beverages', stock: 28, price: 25, sales: 14},
];

export let tablectg: Table1[] = ['Chocolates', 'Toothpaste', 'Chips', 'Beverages' ];

