import { Book } from './../booksdata';
import { Router, ActivatedRoute, ParamMap  } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { AuthService } from './../auth.service';
import { NetService } from './../net.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() id;
  @Output() updateData = new EventEmitter();
  newId;
  iddd;
  detail = [];
  detail1 = [];
  url2 = 'http://localhost:2410/sporticons/details/';
  constructor(private  netService: NetService, private route: ActivatedRoute, private router: Router) { }

  cricketStars() {
    let path = "/stars" ;
    this.router.navigate([path]);
  }


  ngOnInit() {
    this.detail = [];
    this.detail1 = [];

 this.route.paramMap.subscribe(params => {
      this.iddd = params.get('id')
      console.log(this.iddd);
      this.netService.getData(this.url2+this.iddd)
      .subscribe(resp => {
        console.log(resp);
      this.detail.push(resp);
      this.detail1.push(this.detail[0].details);
      console.log(this.detail1);
      console.log(this.detail);
      });

        })

      }


  /*tvArray = [
    {name: 'LG', model: 'FX204', price: '32000' },
    {name: 'Samsung', model: 'SM148', price: '36000' },
    {name: 'Sony', model: 'K4555X', price: '42000' },
    {name: 'Xiaomi', model: 'Mi208Y', price: '24000' },
    {name: 'Panasonic', model: 'PN110', price: '41000' },
  ];



  constructor(private authService: AuthService,
    private route: ActivatedRoute, private router: Router
    ) { }

    add(n) {
      let data =  this.tvArray[n];
      console.log(data);
      this.authService.addToCart(data);

    }
    ngOnInit() {

    }*/


/*@Input() abook: Book;
@Input() val;
@Output() issuedBook = new EventEmitter();

isvalid = true;
  constructor() { }



  issue() {
    console.log(this.abook);
console.log(this.val);
   this.issuedBook.emit(this.val);
  }

  readmore(val) {
this.isvalid = false;
  }
  readless(val) {
    this.isvalid = true;
  }*/

 /* name;
  time;
  extra = 'yes';
  faculty;
  message;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

  }

  create() {
    let path = "/lectures/" + this.name;
    let myparams = {};
    myparams['name'] = this.name;
    myparams['extra'] = this.extra;
    myparams['time'] = this.time;
    myparams['faculty'] = this.faculty;
    myparams['message'] = this.message;
    this.router.navigate([path], {queryParams: myparams});

      }
      newCreate() {
        let path = "/lectures/" + 'ReactJS';
        let myparams = {};
        myparams['name'] = 'ReactJS';
        myparams['extra'] = 'yes';
        myparams['time'] = '9 PM';
        myparams['faculty'] = 'Phil James';
        myparams['message'] = 'Special Topics in React';
        this.router.navigate([path], {queryParams: myparams});

          }*/


          /*newCourse;
  constructor(private route: ActivatedRoute, private router: Router) { }

  submit() {
    let path = '/courses/';
    let myparams = {};
    myparams['newCourse'] = this.newCourse;
    console.log(this.newCourse);
    this.router.navigate([path], {queryParams: myparams});
  }

  ngOnInit() {

  }*/


    }
