import { Product } from './../products';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-getproduct',
  templateUrl: './getproduct.component.html',
  styleUrls: ['./getproduct.component.css']
})
export class GetproductComponent implements OnInit {
@Input() product: Product;
  constructor() { }

  ngOnInit() {
  }

}
