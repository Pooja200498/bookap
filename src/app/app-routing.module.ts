import { EmployeesComponent } from './employees/employees.component';
import { BooksComponent } from './books/books.component';
import { LogoutComponent } from './logout/logout.component';
import { TeamoptionsComponent } from './teamoptions/teamoptions.component';
import { TeamComponent } from './team/team.component';
import { MatchXComponent } from './match-x/match-x.component';
import { BookComponent } from './book/book.component';
import { ByeComponent } from './bye/bye.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HelloComponent} from './hello/hello.component';
import {HomeComponent} from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: 'courses/:coursename',
    component: HelloComponent
  },
  {
    path: 'courses',
    component: HelloComponent
  },
  {
    path: 'stars',
    component: HomeComponent
  },
  {
    path: 'stars/:sports',
    component: HomeComponent
  },
  {
    path: 'course',
    component: BookComponent
  },
  {
    path: 'book',
    component: BookComponent
  },
  {
    path: 'student',
    component: HelloComponent
  },
  {
    path: 'students/:studentname',
    component: ByeComponent
  },
  {
    path: 'extraclass',
    component: BookComponent
  },
  {
    path: 'students',
    component: ByeComponent
  },
  {
    path: 'contact',
    component: BookComponent
  },
  {
    path: 'lectures/:username',
    component: ByeComponent
  },
  {
    path: 'lectures',
    component: ByeComponent
  },
  {
    path: 'hello',
    component: HelloComponent
  },
  {
    path: 'bye/:username',
    component: ByeComponent
  },
  {
    path: 'bye',
    component: ByeComponent
  },
  /*{
    path: 'team',
    component: TeamComponent
  },*/
  {
    path: 'team/:team',
    component: TeamComponent
  },
  {
    path: 'books/:genre',
    component: BooksComponent
  },
  {
    path: 'books',
    component: BooksComponent
  },
  {
    path: 'details/:id',
    component: BookComponent
  },
  {
    path: 'teamOptions',
    component: TeamoptionsComponent
  },
  {
    path: 'teamOptions/:team',
    component: TeamoptionsComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'emps',
    component: EmployeesComponent
  },
  {
    path: 'emps/:location',
    component: EmployeesComponent
  },

 {
    path: '',
    redirectTo: '', pathMatch:'full'
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
