import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {
@Input() team1 ;
@Input() team2 ;
@Output() gameOver = new EventEmitter();
@Output() newArray = new EventEmitter();
scoreTeam1 = 0;
scoreTeam2 = 0;
result = '';


n;
m;
/*arrayTeam1 = [{name: 'France', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0 },
{name: 'England', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
{name: 'Brazil', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
{name: 'Germany', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
{name: 'Argentina', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
];*/

goalTeam1() {

this.scoreTeam1 = this.scoreTeam1 + 1;
}

goalTeam2() {
  this.scoreTeam2 = this.scoreTeam2 + 1;
  }

  matchOver() {
   /* for (var i = 0 ; i < this.arrayTeam1.length ; i++ ) {
      if (this.arrayTeam1[i].name === this.team1 ) {
           this.n = i;
           this.arrayTeam1[i].played = this.arrayTeam1[i].played + 1;
           this.arrayTeam1[i].goalsFor = this.arrayTeam1[i].goalsFor + this.scoreTeam1;
           this.arrayTeam1[i].golasAgainst = this.arrayTeam1[i].golasAgainst + this.scoreTeam2;
      }
      if (this.arrayTeam1[i].name === this.team2 ) {
        this.m = i;
        this.arrayTeam1[i].played = this.arrayTeam1[i].played + 1;
        this.arrayTeam1[i].goalsFor = this.arrayTeam1[i].goalsFor + this.scoreTeam2;
           this.arrayTeam1[i].golasAgainst = this.arrayTeam1[i].golasAgainst + this.scoreTeam1;
   }
    }*/


    if (this.scoreTeam1 > this.scoreTeam2) {
      this.result = this.team1 + ' Won';
      //this.arrayTeam1[this.n].won = this.arrayTeam1[this.n].won + 1;
      //this.arrayTeam1[this.m].lost = this.arrayTeam1[this.m].lost - 1;
    }
    else if (this.scoreTeam1 < this.scoreTeam2) {
      this.result = this.team2 + ' Won';
      //this.arrayTeam1[this.m].won = this.arrayTeam1[this.m].won + 1;
      //this.arrayTeam1[this.n].lost = this.arrayTeam1[this.n].lost - 1;
    }
    else{
      this.result = 'Match Drawn';
      //this.arrayTeam1[this.n].drawn = this.arrayTeam1[this.n].drawn + 1;
      //this.arrayTeam1[this.m].drawn = this.arrayTeam1[this.m].drawn + 1;
    }
this.gameOver.emit({teamOne: this.team1 , teamTwo: this.team2, scoreT1: this.scoreTeam1,
scoreT2: this.scoreTeam2, result: this.result});
//console.log(this.arrayTeam1);
//this.newArray.emit(this.arrayTeam1);
  }

  constructor() { }

  ngOnInit() {
  }

}
