import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-points-table',
  templateUrl: './points-table.component.html',
  styleUrls: ['./points-table.component.css']
})
export class PointsTableComponent implements OnInit {
@Input() pointsTable;
  constructor() { }

  sortByName() {
    this.pointsTable.sort(function(a,b){
  if (a.name < b.name) {
    return -1;
  }
  if (a.name > b.name) {
    return 1;
  }
  return 0;
    } );
  }
  sortByPlayed() {
    this.pointsTable.sort(function(a,b){
   return b.played - a.played;
    } );
  }
  sortByWon() {
    this.pointsTable.sort(function(a,b){
   return b.won - a.won;
    } );
  }
  sortByLost() {
    this.pointsTable.sort(function(a,b){
   return b.lost - a.lost;
    } );
  }
  sortByDrawn() {
    this.pointsTable.sort(function(a,b){
   return b.drawn - a.drawn;
    } );
  }
  sortByGoalsFor() {
    this.pointsTable.sort(function(a,b){
   return b.goalsFor - a.goalsFor;
    } );
  }
  sortByGoalsAgainst() {
    this.pointsTable.sort(function(a,b){
   return b.golasAgainst - a.golasAgainst;
    } );
  }
  sortByPoints() {
    this.pointsTable.sort(function(a,b){
   return b.points - a.points;
    } );
  }

  ngOnInit() {
  }

}
