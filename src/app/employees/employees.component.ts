import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  emps = [
    {
    name: "Amit Kumar",
    department: "Finance",
    designation: "Manager",
    salary: 24000,
    email: "amit.kumar@company.com",
    mobile: "9898346473",
    location: "New Delhi"
    },
    {
    name: "Preeti Sharma",
    department: "Technology",
    designation: "Manager",
    salary: 28500,
    email: "preeti.sharma@company.com",
    mobile: "9898236541",
    location: "New Delhi"
    },
    {
    name: "Vishal Verma",
    department: "Operations",
    designation: "Manager",
    salary: 22100,
    email: "vishal.verma@company.com",
    mobile: "9910346632",
    location: "New Delhi"
    },
    {
    name: "Charu Kumari",
    department: "HR",
    designation: "Manager",
    salary: 23500,
    email: "charu.kumari@company.com",
    mobile: "7023734553",
    location: "New Delhi"
    },
    {
    name: "Puneet Gupta",
    department: "Finance",
    designation: "Trainee",
salary: 14450,
email: "puneet.gupta@company.com",
mobile: "8836436731",
location: "Noida"
},
{
name: "Namita Singh",
department: "Technology",
designation: "Trainee",
salary: 14590,
email: "namita.singh@company.com",
mobile: "9801228812",
location: "Noida"
},
{
name: "Samit Bansal",
department: "Operations",
designation: "Trainee",
salary: 13900,
email: "samit.bansal@company.com",
mobile: "7003551118",
location: "Noida"
},
{
name: "Priya Talwar",
department: "HR",
designation: "Trainee",
salary: 14450,
email: "priya.talwar@company.com",
mobile: "814452341",
location: "Noida"
},
{
name: "Shivam Singh",
department: "Finance",
designation: "Trainee",
salary: 15100,
email: "shivam.singh@company.com",
mobile: "7173958440",
location: "Noida"
},
{
name: "Shelja Prasad",
department: "Technology",
designation: "Trainee",
salary: 15500,
email: "shelja.prasad@company.com",
mobile: "9898346473",
location: "Noida"
},
{
name: "Mithali Dutt",
department: "Finance",
designation: "President",
salary: 68200,
email: "mithali.dutt@company.com",
mobile: "98100346731",
location: "New Delhi"
},
{
name: "Pradeep Kumar",
department: "Technology",
designation: "President",
salary: 84900,
email: "pradeep.kumar@company.com",
mobile: "98254634121",
location: "New Delhi"
},
{
name: "Amit Singh",
department: "Operations",
designation: "President",
salary: 71250,
email: "amit.singh@company.com",
mobile: "98145537842",
location: "New Delhi"
},
{
name: "Garima Rai",
department: "HR",
designation: "President",
salary: 69200,
email: "garima.rai@company.com",
mobile: "998107654387",
location: "New Delhi"
}
];


designationArray = ['Manager', 'Trainee', 'President'];
  departmentsArray = ['Finance', 'Technology', 'Operations', 'HR'];
  tempArray = [];
  printArray = [];

  location = 'All';
  
departments = '';
designation = '';
departmentStructure = null;
designationStructure = null;
departmentsSelected = '';

maxPage = 5;

firstEmp = 0;
secondEmp = 1;
dpt = '';
dsg = '';

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.location = params.get('location');

      if (this.location === null) {
      this.location = 'All';
      }
      this.makeStructs();
    });

    this.route.queryParamMap.subscribe(params => {
      this.departments = params.get('departments');
      this.designation = params.get('designation');

    });
}


  makeStructs() {

    this.departmentsSelected = this.departments ? this.departments : "";
    this.departmentStructure = this.departmentsArray.map(pl1 => ({
      name : pl1,
      selected: false
    }));
    let temp1 = this.departmentsSelected.split(",");
    for ( let i = 0; i < temp1.length; i++ ) {
  let item = this.departmentStructure.find(p1 => p1.name === temp1[i]);
  if (item) item.selected = true;
    }
    this.designationStructure = {
      sports: this.designationArray,
      selected: this.designation ? this.designation : ""
    };
   
    this.optChange();
  }


  previous() {
    this.printArray = [];
    this.firstEmp = this.firstEmp - 1;
    this.secondEmp = this.secondEmp - 1;
    this.printArray.push(this.tempArray[this.firstEmp]);
      this.printArray.push(this.tempArray[this.secondEmp]);

  
    let path;
    if (this.location === 'All') {
      path = '/emps';
     }
       else{
       path = '/emps/' + this.location;
       }
       this.router.navigate([path], {
        queryParams: {
          page: this.secondEmp
        },
        queryParamsHandling: "merge"
      });

  }


  next() {
    this.printArray = [];
    this.firstEmp = this.firstEmp + 1;
    this.secondEmp = this.secondEmp + 1;

    if (this.firstEmp === this.tempArray.length) {
    this.printArray.push(this.tempArray[this.firstEmp]);
this.printArray.push(this.tempArray[1]);
}

    else if (this.secondEmp === this.tempArray.length) {
      this.printArray.push(this.tempArray[this.firstEmp]);
      this.printArray.push(this.tempArray[this.secondEmp]);
    }
    else {
      this.printArray.push(this.tempArray[this.firstEmp]);
      this.printArray.push(this.tempArray[this.secondEmp]);
    }


    let path;
    if (this.location === 'All') {
      path = '/emps';
     }
       else{
       path = '/emps/' + this.location;
       }
       this.router.navigate([path], {
        queryParams: {
          page: this.secondEmp
        },
        queryParamsHandling: "merge"
      });

  }




  optChange() {
    this.firstEmp = 0;
    this.secondEmp = 1;
    this.printArray = [];

    this.tempArray = [];
      let temp1 = this.departmentStructure.filter(p1 => p1.selected);
      let temp2 = temp1.map(p1 => p1.name);
      this.departmentsSelected = temp2.join(",");
      if(this.location === 'All' ) {
        this.tempArray = [...this.emps];
      }
    else {
      this.tempArray = this.emps.filter( a => a.location === this.location );
    }


    if (temp1.length > 0) {
     this.tempArray = this.tempArray.filter(function(a) {

       for (let i = 0; i < temp1.length; i++) {

    if (a.department === temp1[i].name) {
      return a;
    }
       }
     });
    }

    if (this.designationStructure.selected != '') {
     this.tempArray = this.tempArray.filter( a => a.designation === this.designationStructure.selected );
    }

     console.log(this.tempArray);
if (this.tempArray.length > 1) {
this.printArray.push(this.tempArray[this.firstEmp]);
this.printArray.push(this.tempArray[this.secondEmp]);
}
if (this.tempArray.length === 1) {
  this.printArray.push(this.tempArray[0]);
  }

this.maxPage = this.tempArray.length - 2;
let path;
   if (this.location === 'All') {
    path = '/emps';
   }
     else{
     path = '/emps/' + this.location;
     }

     let qparams = {};
      if (this.departmentsSelected) {
        qparams['departments'] = this.departmentsSelected;
      }
        if (this.designationStructure.selected) {
          console.log("groejgre");
        qparams['designation'] = this.designationStructure.selected;
        }
        this.router.navigate([path], { queryParams: qparams });

       if (this.departmentsSelected === null) {
      this.dpt = 'All';
      }
      else {
        this.dpt = this.departmentsSelected;
      }
      if (this.designationStructure.selected === null) {
        this.dsg = 'All';
        }
        else {
          this.dsg = this.designationStructure.selected;
        }
        if (this.departmentsSelected === '') {
          this.dpt = 'All';
          }
          else {
            this.dpt = this.departmentsSelected;
          }
          if (this.designationStructure.selected === '') {
            this.dsg = 'All';
            }
            else {
              this.dsg = this.designationStructure.selected;
            }

    }

}
