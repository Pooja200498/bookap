import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.css']
})
export class QuestionFormComponent implements OnInit {
  @Input() questions;
  @Output() updatedData = new EventEmitter();
  name;

submit() {
this.updatedData.emit(this.name);
}

  ngOnInit() {

  }

/*@Input() questions;
@Output() updatedData = new EventEmitter();
@Output() updatedData1 = new EventEmitter();
@Input() editable;
  question;
  optionA;
  optionB;
  optionC;
  optionD;
  correctOption;
  Property1 = true;
  Property2 = false;

  submit() {

this.updatedData.emit({question: this.question, optionA : this.optionA, optionB: this.optionB,
optionC: this.optionC, optionD: this.optionD, correctOption: this.correctOption});

  }

  edit() {
    this.questions[this.editable].question = this.question;
    this.questions[this.editable].optionA = this.optionA;
    this.questions[this.editable].optionB = this.optionB;
    this.questions[this.editable].optionC = this.optionC;
    this.questions[this.editable].optionD = this.optionD;
    this.questions[this.editable].correctOption = this.correctOption;
this.updatedData1.emit();
  }

  constructor() { }

  ngOnInit() {
if (this.editable > -1) {
this.Property1 = false;
this.Property2 = true;
this.question = this.questions[this.editable].question;
this.optionA = this.questions[this.editable].optionA;
this.optionB = this.questions[this.editable].optionB;
this.optionC = this.questions[this.editable].optionC;
this.optionD = this.questions[this.editable].optionD;
this.correctOption = this.questions[this.editable].correctOption;

}
  }*/

}
