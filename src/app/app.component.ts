import { Subject } from 'rxjs';
import { CourseInfo } from './courseinfo';
import { HttpClient } from '@angular/common/http';
import { Course, courseList, cityList, City } from './course';
import { Book, bookdb } from "./booksdata";
import { Question, questiondb } from "./questions";

import { Component , OnInit } from "@angular/core";
import { Students } from "./students";
import { Student, studentList } from './course';
//import { Animal, animaldb } from './';
import { NetService } from './net.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthService } from './auth.service';
import { type } from 'os';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {

/*arr = [{id: 121,sent: false,from: "tweets@twitter.com",to: "jack@test.com",
subject: "18 tweets from those you follow",
text: "Go to your twitter page and see the tweets from those you follow.",
folder: "Social"},
{id: 141,sent: true,from: "jack@test.com",to: "mary@test.com",
subject: "Bug 461 in Customer Flow",
text: "When thecheckbox is left unchecked and the option Important is selected in the dropdown, clicking on Submit, shows no results.",
folder: "Sent"},
{id: 158,sent: false,from: "email@facebook.com",to: "jack@test.com",
subject: "New post from William Jones",
text:"William Jones has just uploaded a new post -How i loved the Avengers Endgame.",
folder: "Social"},
{id: 177,sent: true,from: "jack@test.com",to: "williams@test.com",
subject: "Movie tomorrow",text: "Avengers Endgame is releasing tomorrow. Wanna see.",
folder: "Sent"},
{id: 179,sent: false,from: "williams@test.com",to: "jack@test.com",
subject: "Re: Movie tomorrow",
text:"The movie is supposed to be a blast. Lets do the 8:30 show. Want to have a quick bite before.",
folder: "Inbox"},
{id: 194,sent: false,from: "retweet@twitter.com",to: "jack@test.com",
subject: "Your tweet has been retweeted by Thomas",
text:"Your tweet on the Marvel Superheroes and Avengers has been retweeted bt Thomas. It has now 41 retweets and 27 likes.",
folder: "Social"},
{id: 204,sent: true,from: "jack@test.com",to: "jack@test.com",subject: "To do on Friday",
text: "Test the bugs on the employee form in Release 0.7.9 and fix them.",folder: "Work"},
{id: 255,sent: true,from: "mary@test.com",
to: "jack@test.com",subject: "Release 0.8.4 deployed",
text: "Release 0.8.4 has been deployed in the test environment.",
folder: "Inbox"},
{id: 278,sent: false,from: "mary@test.com",to: "jack@test.com",
subject: "Re: Bug 461 in Customer Flow",
text:"The bug has been fixed in the release 0.8.7. \nPlease test the issue and close it.\nCan you do it but tomorrow\nMary",
folder: "Work"},
{id: 281,sent: true,from: "jack@test.com",to: "mary@test.com",
subject: "Re: Re: Bug 461 in Customer Flow",
text: "Bug 461 has been closed.\nRegards,\nJack",folder: "Sent"},
{id: 289,sent: false,from: "email@facebook.com",to: "jack@test.com",
subject: "5 Shares, 2 Posts from your friends",
text:"Jack, while you were away, your friends are having fun on Facebook.\nDon't miss their posts.\nKeep up with your friends.",
folder: "Social"}
];

Property1 = false;
Property2 = false;

typeArray = [
{type : 'Inbox', numbers : 0},
{type : 'Sent', numbers : 0},
{type : 'Drafts', numbers : 0},
{type : 'Work', numbers : 0},
{type : 'Social', numbers : 0},
];

printType = '';
newArr = [];
to = '';
subject = '';
message = '';
id = 0;

printMessages(n) {
  this.Property1 = false;
  this.Property2 = true;
this.printType = this.typeArray[n].type;
this.newArr = [];
for ( let i = 0; i < this.arr.length; i++ ) {
if (this.arr[i].folder === this.printType ) {
this.newArr.push(this.arr[i]);
}
}

}

compose() {
this.Property1 = true;
this.Property2 = false;
}

send() {
this.arr.push({id: this.id, sent: true, from: 'jack@test.com', to: this.to, subject: this.subject,
text: this.message, folder: 'Sent'  });
this.Property2 = true;
this.Property1 = false;
this.printMessages(1);
}


counts() {
this.id = this.arr[length].id;
for ( let i = 0; i < this.arr.length; i++ ) {
  for ( let j = 0; j < this.typeArray.length; j++ ) {
if (this.arr[i].folder === this.typeArray[j].type ) {
this.typeArray[j].numbers = this.typeArray[j].numbers + 1;
}
  }
}

}


ngOnInit() {
this.counts();
}*/


/*teamArray = ['Superman', 'Wonder Woman', 'Thor', 'Captain America', 'Spiderman'];
membersArray = [ 'Jack', 'Anita', 'Mary', 'Steve', 'Bob', 'Dave', 'Edwards', 'Joe', 'Felix',
'Nate', 'Peter', 'Pam', 'Alice', 'Wendy', 'Tim', 'James', 'Kathy', 'Anna'];

newMembersArray = [...this.membersArray];

teamsAndMembers = [];
selTeams = [];
teams = '';
members = '';
Property1 = false;
Property2 = false;
Property3 = false;
selMember = '';
selTeam = '';

mainView() {
this.Property1 = true;
this.Property2 = false;
this.Property3 = false;
}

addMember() {
  var count = 0;
this.teamsAndMembers.push({team: this.teams , member: this.members});
for (let i = 0 ; i < this.selTeams.length ; i++ ) {
if (this.selTeams[i] === this.teams) {
count++;
}
}
if (count === 0) {
this.selTeams.push(this.teams);
}

for (let i = 0; i < this.newMembersArray.length ; i++ ) {
if ( this.members === this.newMembersArray[i] ) {
  this.newMembersArray.splice(i, 1);
}
}
this.members = '';

}

searchMember() {
  this.Property1 = false;
  this.Property2 = true;
  this.Property3 = false;
}

change() {
  this.selTeam = '';
  for (let i = 0; i < this.teamsAndMembers.length ; i++ ) {
    if ( this.selMember === this.teamsAndMembers[i].member ) {
this.selTeam = this.teamsAndMembers[i].team;
    }
    }
    if (this.selTeam === '') {
this.selTeam = 'Not in any team';
  }
}

memberView() {
  this.Property1 = false;
  this.Property2 = false;
  this.Property3 = true;
}

  ngOnInit() {

  }*/


  genre = ['Children', 'Mystery', 'Management', 'Self	Help'];
binding = '';


  ngOnInit() {

  }



/*arr = [[2, 3, 4, 6, 7], [3, 4, 5], [5, 10], [14], [4, 8, 12, 16]];
newArr = [];
idArr = [];

ngOnInit() {

this.stats();

}

stats() {
  this.newArr = [];
  let details;

  for (let i = 0; i < this.arr.length; i++) {
      var max = this.arr[i][0];
      var min = this.arr[i][0];
      var table = '';
      var sum = 0;
  for (let j = 0; j < this.arr[i].length; j++) {

      var count = this.arr[i].length;
      sum = sum + this.arr[i][j];
      if (max < this.arr[i][j]) {
          max = this.arr[i][j];
      }
      else {
          max = max;
      }

      if (min > this.arr[i][j]) {
          min = this.arr[i][j];
      }
      else {
          min = min;
      }
  }
  details = {sum: sum, count: count, max: max, min: min};
  this.newArr.push(details);
  }
  console.log(this.newArr);
}

add(n) {
  for (let i = 0; i < this.arr.length; i++) {
    for (let j = 0; j < this.arr[i].length; j++) {
        this.arr[i][j] = this.arr[i][j] + n;
    }
}
this.stats();
}


insert22() {
  let multarr = [];
  for (let i = 0; i < this.arr.length; i++) {
     let arr1 = [];
     multarr.push(arr1);
     for (let j = 0; j < this.arr[i].length; j++) {
     arr1.push(this.arr[i][j]);
     }
     arr1.push(22);
  }
  this.arr = multarr;
  this.stats();
}

insert0() {

  let multarr = [];
  for (let i = 0; i < this.arr.length; i++) {
    let arr1 = [];
    multarr.push(arr1);
    arr1.push(0);
    for (let j = 0; j < this.arr[i].length; j++) {
    arr1.push(this.arr[i][j]);
    }

 }
 this.arr = multarr;
 this.stats();
}


remove4OrLess() {
  let count = 0;
  while (count < 3) {
for (let i = 0; i < this.arr.length; i++) {
       for (let j = 0; j < this.arr[i].length; j++) {
           let a = this.arr[i][j];
           if (a < 5) {
               this.arr[i].splice(j, 1);

           }
       }

   }
   count++;
  }
  this.stats();
}

doubleValue(n) {
 let a = n.substring(0,1);
 let b = n.substring(1);
 this.arr[a][b] = this.arr[a][b] * 2;
}*/



/*questions = [
  {id: 1, qnText: "What is the square of 9",A: "90", B: "72", C: "81", D: "99", ans: "C"},
  {id: 2, qnText: "What is the 11*16", A: "160", B: "176", C: "204", D: "166", ans: "B"},
  {id: 3, qnText: "Which of the following is not a power of 2",A: "0",B: "1",C: "2",D: "8",ans:"A"},
  {id: 4, qnText: "log 1 is equal to",A: "1", B: "10", C: "-1", D: "0", ans: "D"},
  {id: 5, qnText: "log(ab) is equal to",A: "(loga) + (logb)", B: "b(loga)",
   C: "a(logb)", D: "(loga)(logb)", ans: "A"},
  {id: 6, qnText: "The square root is equal to",A: "1.0", B: "1.25",
   C: "1.414", D: "1.462", ans: "B"},
   {id: 7, qnText: "The binary representation of 10 is",A: "0110", B: "1001",
    C: "1010", D: "1100", ans: "C"},
    {id: 8, qnText: "11111 in binary represents ",A: "27", B: "15",
    C: "41", D: "31", ans: "D"},
    {id: 9, qnText: "The absolute value of -10.5 is equal to ",A: "-10.5", B: "10.5",
    C: "10", D: "11", ans: "B"},
    {id: 10, qnText: "The roots of the equation of (x-2)(x+3) = 0 are", A: "2, -3",
     B: "2, 3", C: "-2, 3", D: "-2, -3", ans: "A"}
    ];

    Property1 = true;
    Property2 = false;
    Property3 = false;
    Property4 = false;
    Property5 = false;
    selName = '';
    name = '';
    questionPapers = [];
    selPaper = [];


  questionBank() {
    console.log(this.questions);
    this.Property1 = false;
    this.Property2 = false;
    this.Property3 = true;
    this.Property4 = false;
    this.Property5 = false;
  }

  home() {
    this.Property1 = true;
    this.Property2 = false;
    this.Property3 = false;
    this.Property4 = false;
    this.Property5 = false;
  }

  createQuestionPaper() {
    this.Property1 = false;
    this.Property2 = true;
    this.Property3 = false;
    this.Property4 = false;
    this.Property5 = false;
  }

  updatedData(s) {
    this.Property1 = true;
    this.Property2 = false;
    this.Property3 = false;
    this.Property4 = false;
    this.Property5 = false;
console.log(this.questions);
let temp1 = this.questions.filter(p1 => p1.selected);
console.log(temp1);
this.questionPapers.push({name: s, selques: temp1});
console.log(this.questionPapers);
for (let i = 0; i < this.questions.length ; i++) {
  this.questions[i].selected = false;
}
  }


  viewQuestionPapers() {
    this.Property1 = false;
    this.Property2 = false;
    this.Property3 = false;
    this.Property4 = true;
    this.Property5 = false;
    console.log(this.selName);
  }

  selpaper() {
    this.Property5 = true;
    for (let i = 0; i < this.questionPapers.length; i++) {
      if (this.questionPapers[i].name === this.selName) {
        this.name = this.questionPapers[i].name;
        this.selPaper = this.questionPapers[i].selques;
      }
    }
    console.log(this.selPaper);

  }


ngOnInit() {
for (let i = 0; i < this.questions.length ; i++) {
this.questions[i]['selected'] = false;
}
}*/



/*questions = [];
text = '';

Property1 = true;
Property2 = false;
Property3 = false;
editable = -1;

  addQuestion() {
this.Property1 = false;
this.Property2 = true;
  }

  home() {
    this.Property1 = true;
    this.Property2 = false;
    this.Property3 = false;
  }

  questionBank() {
    if (this.questions.length < 1) {
this.text = 'No questions have been added so far';
    }
    else{
      this.text = '';
    }
    console.log(this.questions);
    this.Property1 = false;
    this.Property2 = false;
    this.Property3 = true;

  }

  updatedData1() {
this.Property1 = false;
this.Property2 = false;
this.Property3 = true;
  }

  updatedData(s) {
this.questions.push(s);
this.Property1 = false;
this.Property2 = false;
this.Property3 = true;
  }

  deleteData(i) {
    this.questions.splice(i, 1);
    if (this.questions.length < 1) {
      this.text = 'No questions have been added so far';
    }
  }

  edit(i) {
this.editable = i;
this.Property1 = false;
this.Property2 = true;
this.Property3 = false;
  }

  ngOnInit() {

  }*/


  /*Assignment 3 */

  /*constructor(private authService:AuthService){}


 products = [
    {
    code: "PEP1253", price: 20, brand: "Pepsi", category: "Food",
    specialOffer: false, limitedStock: false, quantity: 25
    },
    {
    code: "MAGG021", price: 25, brand: "Nestle", category: "Food",
    specialOffer: true, limitedStock: true, quantity: 10
    },
    {
    code: "LEV501", price: 1000, brand: "Levis", category: "Apparel",
    specialOffer: true, limitedStock: true, quantity: 3
    },
    {
    code: "CLG281", price: 60, brand: "Colgate", category: "Personal Care",
    specialOffer: true, limitedStock: true, quantity: 5
    },
    {
    code: "MAGG451", price: 25, brand: "Nestle", category: "Food",
    specialOffer: true, limitedStock: true, quantity: 0
    },
    {
    code: "PAR250", price: 40, brand: "Parachute", category: "Personal Care",
    specialOffer: true, limitedStock: true, quantity: 5
    }
    ];

    editableData = -1;
    quantity = 0;
    value = 0;
    Property1 = true;
    Property2 = false;
    Property3 = false;
    Property4 = false;


    addNewProduct() {
this.Property1 = false;
this.Property2 = true;
this.Property3 = false;
this.Property4 = false;
    }

    updatedData(data) {
this.products.push(data);
      this.Property1 = true;
this.Property2 = false;
this.Property3 = false;
this.Property4 = false;
    }

    updatedData1() {
      this.Property1 = true;
      this.Property2 = false;
      this.Property3 = false;
      this.Property4 = false;
      this.quantity = 0;
      this.value = 0;
      for (let i = 0; i < this.products.length; i++) {
        this.quantity = this.quantity +  this.products[i].quantity;
        console.log(this.quantity);
        this.value = this.value +  this.products[i].quantity * this.products[i].price;
         }
    }

    receiveStocks() {
      this.Property1 = false;
      this.Property2 = false;
      this.Property3 = true;
      this.Property4 = false;
    }

    edit(i) {
this.editableData = i;
this.Property1 = false;
      this.Property2 = false;
      this.Property3 = false;
      this.Property4 = true;
    }

    ngOnInit() {
      for (let i = 0; i < this.products.length; i++) {
     this.quantity = this.quantity +  this.products[i].quantity;
     this.value = this.value +  this.products[i].quantity * this.products[i].price;
      }

    }*/





  /*navlinks: {link: string, active: string, txt: any } [];
cartArray = [];
constructor(private authService:AuthService){}

ngOnInit() {
  this.authService.loginStatusObs$.subscribe(
    status => {
      console.log("subscribe::"+status);
      this.updateNavBar();
    }
  );
  this.updateNavBar();
}

   updateNavBar() {
    this.cartArray = this.authService.cartArray;
      var mobile = {link: '/login', active: 'active', txt: 'Mobile' };
      var laptop = {link: '/logout', active: 'active', txt: 'Laptop' };
      var tv = {link: '/book', active: 'active', txt: 'TV' };
      var cart = {link: '/hello', active: 'active', txt: 'Cart'  };
      if (this.authService.loginStatus)
      this.navlinks = [mobile, laptop, tv, cart];
      else
      this.navlinks = [mobile, laptop, tv];
    }*/


  /*ngOnInit() {
this.authService.loginStatusObs$.subscribe(
  status => {
    console.log("subscribe::"+status);
    this.updateNavBar();
  }
);
this.updateNavBar();

  }

  updateNavBar() {
    var home = {link: '/login', active: 'active', txt: 'Home' };
    var angular = {link: '/logout', active: 'active', txt: 'Angular' };
    if (this.authService.loginStatus)
    this.navlinks = [home, angular];
    else
    this.navlinks = [home];
  }*/



  /*constructor(private route: ActivatedRoute, private router: Router) { }*/


  /*studentOption = '';
  courseOption= '';

  courseDetails() {

console.log(this.courseOption);
if(this.courseOption === 'Course List') {
  let path = '/courses/';
  let myparams1 = {};
    myparams1['propertyOption'] = 'A';
    this.router.navigate([path], {queryParams: myparams1});
}
if(this.courseOption === 'Add Course') {
  let path = '/course/';
  let myparams1 = {};
  myparams1['studentOption'] = this.studentOption;
  console.log(this.studentOption);
  this.router.navigate([path], {queryParams: myparams1});
}

}

studentDetails() {
  if(this.studentOption === 'Student List') {
    let path = '/students/';
    let myparams1 = {};
    myparams1['propertyOption'] = 'B';
    this.router.navigate([path], {queryParams: myparams1});
  }
  if(this.studentOption === 'Add Student') {
    let path = '/student/';
    let myparams1 = {};
    myparams1['propertyOption'] = 'B';
    console.log(this.studentOption);
    this.router.navigate([path], {queryParams: myparams1});
  }

} */

/*Assignment 2 */
/*teams = ['France', 'England', 'Brazil', 'Germany', 'Argentina'];

arrayTeam1 = [{name: 'France', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0 },
{name: 'England', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
{name: 'Brazil', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
{name: 'Germany', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
{name: 'Argentina', played: 0 , won: 0, lost: 0, drawn: 0 ,goalsFor:0, golasAgainst:0, points: 0},
];

n;
m;

matches = [];
Property = true;
Property1 = false;
Property2 = false;
Property3 = false;
newArray = [];

newMatch() {
  this.Property3 = true;
  this.Property1 = false;
this.Property2 = false;
}

changeProperties() {
  this.Property = false;
}

allMatches() {
this.Property1 = true;
this.Property2 = false;
this.Property3 = false;
}

pointsTable() {
  this.Property2 = true;
  this.Property3 = false;
  this.Property1 = false;
}

changedData(data) {
console.log("hwsw");
this.matches.push(data);

for (var i = 0 ; i < this.arrayTeam1.length ; i++ ) {
  if (this.arrayTeam1[i].name === data.teamOne ) {
       this.n = i;
       this.arrayTeam1[i].played = this.arrayTeam1[i].played + 1;
       this.arrayTeam1[i].goalsFor = this.arrayTeam1[i].goalsFor + data.scoreT1;
       this.arrayTeam1[i].golasAgainst = this.arrayTeam1[i].golasAgainst + data.scoreT2;
  }
  if (this.arrayTeam1[i].name === data.teamTwo ) {
    this.m = i;
    this.arrayTeam1[i].played = this.arrayTeam1[i].played + 1;
    this.arrayTeam1[i].goalsFor = this.arrayTeam1[i].goalsFor + data.scoreT2;
       this.arrayTeam1[i].golasAgainst = this.arrayTeam1[i].golasAgainst + data.scoreT1;
}
}

if (data.scoreT1 > data.scoreT2) {
  this.arrayTeam1[this.n].won = this.arrayTeam1[this.n].won + 1;
  this.arrayTeam1[this.m].lost = this.arrayTeam1[this.m].lost + 1 ;
}
else if (data.scoreT1 < data.scoreT2) {
  this.arrayTeam1[this.m].won = this.arrayTeam1[this.m].won + 1;
  this.arrayTeam1[this.n].lost = this.arrayTeam1[this.n].lost + 1;
}
else {
  this.arrayTeam1[this.m].drawn = this.arrayTeam1[this.m].drawn + 1;
  this.arrayTeam1[this.n].drawn = this.arrayTeam1[this.n].drawn + 1;
}

for (var i = 0 ; i < this.arrayTeam1.length ; i++ ) {
this.arrayTeam1[i].points = (this.arrayTeam1[i].won * 3) + (this.arrayTeam1[i].drawn * 1) +
(this.arrayTeam1[i].lost * 0);

}

this.Property = true;
console.log(this.matches);
this.Property3 = false;
console.log(this.arrayTeam1);

}


editDetails(s) {
this.newArray = s;
console.log(this.newArray);
}*/




/*url1 = 'https://httpstat.us/500';
url2 = 'https://httpstat.us/402';
url3 = 'https://httpstat.us/400';
url4 = 'https://httpstat.us/403';

error: string;
names: string[];

constructor(private http: HttpClient) {}

clear() {
  this.error = null;
  this.names = null;
}

fetch4(): void {
  this.clear();
  this.http.get(this.url4).subscribe(
    data => {
      console.log(data);
      this.names = <string[]>data;
    },
    error => {
      console.log(error);
      this.error = error.statusText + ' - Not allowed to access';
    }
      );
    }


    fetch1(): void {
      this.clear();
      this.http.get(this.url1).subscribe(
        data => {
          console.log(data);
          this.names = <string[]>data;
        },
        error => {
          console.log(error);
          this.error = error.statusText + ' - Server threw an exception';
        }
          );
        }

        fetch2(): void {
          this.clear();
          this.http.get(this.url2).subscribe(
            data => {
              console.log(data);
              this.names = <string[]>data;
            },
            error => {
              console.log(error);
              this.error = error.statusText + ' - Server is demanding some payment';
            }
              );
            }

            fetch3(): void {
              this.clear();
              this.http.get(this.url3).subscribe(
                data => {
                  console.log(data);
                  this.names = <string[]>data;
                },
                error => {
                  console.log(error);
                  this.error = error.statusText + ' - Server did not understand the request';
                }
                  );
                }*/



/*NG-B1#5 task 1 code */
/*urlSring: string = 'https://www.mocky.io/v2/5c8940da2f0000375dec9762';
errUrlSring: string = 'https://www.mocky.io/v2/5c8913332f00008013ec962d';

error: string;
names: string[];

constructor(private http: HttpClient) {}

clear() {
  this.error = null;
  this.names = null;
}

  fetch(): void {
this.clear();
this.http.get(this.urlSring).subscribe(
  data => {
    console.log(data);
    this.names = <string[]>data;
  },
  error => {
    console.log(error);
    this.error = error.message;
  }
    );
  }

fetchErr(): void {
  this.clear();
  this.http.get(this.errUrlSring).subscribe(
    data => {
      console.log(data);
      this.names = <string[]>data;
    },
    error => {
      console.log(error);
      this.error = error.message;
    }
      );
}*/





 /* selValue: string = '';
url1 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/oqb1u';
url2 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/11tucy';
url3 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/f7dde';

strData: string ;
name: string = '';
data: any;

  constructor(private netService: NetService) {}

  getData() {
console.log("hbhw");
    if (this.name === 'VII-A') {
      console.log("a");
    this.netService.getData(this.url1)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
      this.data = resp;
      console.log(this.data);
    });
  }
  else if (this.name === 'VII-B') {
    console.log("b");
    this.netService.getData(this.url2)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
      this.data = resp;
      console.log(this.data);
    });
  }
  else if (this.name === 'VII-C') {
    console.log("c");
    this.netService.getData(this.url3)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
      this.data = resp;
      console.log(this.data);
    });
  }
}*/


  /*urlPost = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins';
  name1: string;
  brand1: string;
  qty1: string;
  strData: string;
  clist = [];
  constructor(private netService: NetService) {}

  postData() {
    var data1 = {};
    data1['productid'] = this.name1;
    data1['brand'] = this.brand1;
    data1['qty'] = this.brand1;
   this.clist.push(data1);

   this.netService.postData( this.urlPost, this.clist)
    .subscribe(postresp => {
      this.strData = JSON.stringify(postresp);
    });
  }*/



  /*NG-B1#3 task 1 */
/*  urlPost = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins';
clist = ['India', 'France', 'USA', 'Japan'];
strData: string;

  constructor(private netService: NetService) {}

  postData() {
    this.netService.postData( this.urlPost, this.clist)
    .subscribe(postresp => {
      this.strData = JSON.stringify(postresp);
    });
  }*/



  /*NG-B1#2B task 4 code */
  /*url1 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/18py7w';
  strData: string;
  data: any;

  constructor(private netService: NetService) {}

  getCourse() {
    this.netService.getData(this.url1)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
      this.data = resp;
      console.log(this.data);
    });
  }*/



  /*NG-B1#2B task 1 */
/*  url1 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/1g7sq4';
strData: string;
data: any;


constructor(private netService: NetService) {}

getNames() {
  this.netService.getData(this.url1)
  .subscribe(resp => {
    this.strData = JSON.stringify(resp);
    this.data = resp;
  });
}*/



  /*NG-B1#2A task 2 code */
/*  url1 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/1g7sq4';
  url2 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/1c3wng';
  url3 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/16r07g';
  url4 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/18py7w';

  strData: string ;
  selValue: string;

  constructor(private netService: NetService) {}

  getData() {

    if (this.selValue === 'Names') {
    this.netService.getData(this.url1)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
    });
  }
  else if (this.selValue === 'Numbers') {
    this.netService.getData(this.url2)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
    });
  }
  else if (this.selValue === 'Course') {
    this.netService.getData(this.url3)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
    });
  }
 else if (this.selValue === 'All Courses') {
    this.netService.getData(this.url4)
    .subscribe(resp => {
      this.strData = JSON.stringify(resp);
    });
  }

  }*/




/*NG-B1#2A task 1 code*/
/* url1 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/1g7sq4';
url2 = 'https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/bins/16r07g';

strData: string ;
constructor(private netService: NetService) {}

getData() {

  this.netService.getData(this.url1)
  .subscribe(resp => {
    this.strData = JSON.stringify(resp);
  });


}*/




/*
  products	=	[
    {	id:	"PEP110",	name:	"Pepsi",	category:	"Food",	stock:	"yes"	},
    {	id:	"CLO876",	name:	"Close	Up",	category:	"Toothpaste",	stock:	"no"	},
    {	id:	"PEA531",	name:	"Pears",	category:	"Soap",	stock:	"arriving"	},
    {	id:	"LU7264",	name:	"Lux",	category:	"Soap",	stock:	"yes"	},
    {	id:	"COL112",	name:	"Colgate",	category:	"Toothpaste",	stock:	"no"	},
    {	id:	"DM881",	name:	"Dairy	Milk",	category:	"Food",	stock:	"arriving"	},
    {	id:	"LI130",	name:	"Liril",	category:	"Soap",	stock:	"yes"	},
    {	id:	"PPS613",	name:	"Pepsodent",	category:	"Toothpaste",	stock:	"no"	},
    {	id:	"MAG441",	name:	"Maggi",	category:	"Food",	stock:	"arriving"	},
    {	id:	"PNT560",	name:	"Pantene",	category:	"Shampoo",	stock:	"no"	},
    {	id:	"KK219",	name:	"KitKat",	category:	"Food",	stock:	"arriving"	},
    {	id:	"DOV044",	name:	"Dove",	category:	"Soap",	stock:	"yes"	}
];

categories = [ "Food", "Toothpaste", "Soap", 'Shampoo'];
stocks = ['yes', 'no', 'arriving'];

namesStructure = null;
newArray = null;
namesSelected = '';
filteredNames = null;
newfilteredNames = null;
namesRadioStructure = null;
nameRadioSelected = 'All';

ngOnInit() {
 this.updatedValues();
}

updatedValues() {
    console.log(this.namesStructure);
    this.namesStructure = this.categories.map(n1 => ({
      name: n1,
      selected: false
      }));

      this.newArray = this.products.map(n1 => ({
        name: n1.name,
        id: n1.id,
        stock: n1.stock,
        category: n1.category,
        selected: false
        }));

        this.namesRadioStructure = {
          names: this.stocks,
          selected: this.nameRadioSelected
        };
  }

  optChange1() {
    this.newfilteredNames = [];
    this.filteredNames = [];
    this.nameRadioSelected = this.namesRadioStructure.selected;
    console.log(this.nameRadioSelected);
    this.filteredNames = this.namesStructure.filter(n1 => n1.selected);
    let arrayNames = this.filteredNames.map(n1 => n1.name);
    console.log(this.newArray);
    this.namesSelected = arrayNames.join(',');
console.log(this.newArray);

console.log(this.filteredNames);

      for ( let i = 0; i < this.filteredNames.length; i++ ) {
        for ( let j = 0; j < this.newArray.length; j++ ) {
          if (this.newArray[j].category === this.filteredNames[i].name ) {
            this.newArray[j].selected = true;
          }
        }
      }

    console.log(this.newArray);
    this.newfilteredNames = this.newArray.filter(n1 => n1.selected);
console.log(this.newfilteredNames);
if (this.nameRadioSelected != 'All') {
for ( let j = 0; j < this.newfilteredNames.length; j++ ) {
  if (this.newfilteredNames[j].stock === this.nameRadioSelected) {
    this.newfilteredNames[j].selected = true;
  }
  else{
    this.newfilteredNames[j].selected = false;
  }

  }
}
  console.log(this.newfilteredNames);
  this.newfilteredNames = this.newfilteredNames.filter(n1 => n1.selected);
console.log(this.newfilteredNames);
    }*/





  /*NG-A6#2 task 2.2  */
  /*products =	[
    {	id:	"PEP110",	name:	"Pepsi",	category:	"Food",	stock:	true	},
    {	id:	"CLO876",	name:	"Close	Up",	category:	"Toothpaste",	stock:	false	},
    {	id:	"PEA531",	name:	"Pears",	category:	"Soap",	stock:	true	},
    {	id:	"LU7264",	name:	"Lux",	category:	"Soap",	stock:	false	},
    {	id:	"COL112",	name:	"Colgate",	category:	"Toothpaste",	stock:	true	},
    {	id:	"DM881",	name:	"Dairy	Milk",	category:	"Food",	stock:	false	},
    {	id:	"LI130",	name:	"Liril",	category:	"Soap",	stock:	true	},
    {	id:	"PPS613",	name:	"Pepsodent",	category:	"Toothpaste",	stock:	false	},
    {	id:	"MAG441",	name:	"Maggi",	category:	"Food",	stock:	true	}
];
categories = [ "Food", "Toothpaste", "Soap"];

namesRadioStructure = null;
nameRadioSelected = '';
filteredNames = [];

ngOnInit() {
  this.updateValues();
    }

    updateValues() {
      this.namesRadioStructure = {
        names: this.categories,
        selected: this.nameRadioSelected
      };
    }
  optChange() {
    this.filteredNames = [];
      this.nameRadioSelected = this.namesRadioStructure.selected;
      console.log(this.nameRadioSelected);
      for ( let i = 0; i < this.products.length; i++ ) {
      if (this.products[i].category === this.nameRadioSelected) {
        this.filteredNames.push(this.products[i]);
      }
      }
    }*/



  /*NG-A6#2 task 1.3*/
  /*names = ['Jack', 'Steve', 'William', 'Kathy', 'Edward'];
  namesIds = [
    {name: 'Jack', id: 'AB455'},
    {name: 'Steve', id: 'GM072'},
    {name: 'William', id: 'CX499'},
    {name: 'Kathy', id: 'MM746'},
    {name: 'Edward', id: 'KT108'},
  ];

  namesRadioStructure = null;
  nameRadioSelected = '';

  ngOnInit() {
this.updateValues();
  }

  updateValues() {
    this.namesRadioStructure = {
      namesIds: this.namesIds,
      selected: this.nameRadioSelected
    };
  }
optChange() {
    this.nameRadioSelected = this.namesRadioStructure.selected;
  }*/


  /*NG-A6#1 task 3 code */
 /* students = [
    {name: 'James', course: 'Angular'},
    {name: 'Mary', course: 'Python'},
    {name: 'Bob', course: 'Angular'},
    {name: 'Pam', course: 'Android'},
    {name: 'Steve', course: 'Angular'},
    {name: 'William', course: 'Python'},
    {name: 'Julia', course: 'Android'},
     {name: 'Matt', course: 'Java'},
     {name: 'Martin', course: 'Java'},
      {name: 'Katherine', course: 'Angular'},
      {name: 'Nick', course: 'Python'},
     {name: 'Tim', course: 'Java'},
  ];
  courses = [ 'Angular', 'Android', 'Python', 'Java'];


  namesStructure = null;
  newArray = null;
  namesSelected = '';
  filteredNames = null;
  newfilteredNames = null;
  Property = false;
  ngOnInit() {
    this.updatedValues();

    }

    updatedValues() {
      console.log(this.namesStructure);
      this.namesStructure = this.courses.map(n1 => ({
        name: n1,
        selected: false
        }));

        this.newArray = this.students.map(n1 => ({
          name: n1.name,
          course: n1.course,
          selected: false
          }));
    }

    optChange() {
  this.Property = true;
      this.filteredNames = this.namesStructure.filter(n1 => n1.selected);

      for ( let i = 0; i < this.filteredNames.length; i++ ) {
        for ( let j = 0; j < this.newArray.length; j++ ) {
         if (this.newArray[j].course === this.filteredNames[i].name) {
           this.newArray[j].selected = true;
         }
        }
      }
      this.newfilteredNames = this.newArray.filter(n1 => n1.selected);
      }*/




  /*NG-A6#1 task 2.2 code*/
 /* products =	[
    {	id:	"PEP110",	name:	"Pepsi",	category:	"Food",	stock:	true	},
    {	id:	"CLO876",	name:	"Close	Up",	category:	"Toothpaste",	stock:	false	},
    {	id:	"PEA531",	name:	"Pears",	category:	"Soap",	stock:	true	},
    {	id:	"LU7264",	name:	"Lux",	category:	"Soap",	stock:	false	},
    {	id:	"COL112",	name:	"Colgate",	category:	"Toothpaste",	stock:	true	},
    {	id:	"DM881",	name:	"Dairy	Milk",	category:	"Food",	stock:	false	},
    {	id:	"LI130",	name:	"Liril",	category:	"Soap",	stock:	true	},
    {	id:	"PPS613",	name:	"Pepsodent",	category:	"Toothpaste",	stock:	false	},
    {	id:	"MAG441",	name:	"Maggi",	category:	"Food",	stock:	true	}
];

categories = [ "Food", "Toothpaste", "Soap"];

namesStructure = null;
newArray = null;
namesSelected = '';
filteredNames = null;
newfilteredNames = null;
ngOnInit() {

  this.updatedValues();

  }

  updatedValues() {
    console.log(this.namesStructure);
    this.namesStructure = this.categories.map(n1 => ({
      name: n1,
      selected: false
      }));

      this.newArray = this.products.map(n1 => ({
        name: n1.name,
        id: n1.id,
        stock: n1.stock,
        category: n1.category,
        selected: false
        }));

  }

  optChange() {

    this.filteredNames = this.namesStructure.filter(n1 => n1.selected);
    let arrayNames = this.filteredNames.map(n1 => n1.name);
    this.namesSelected = arrayNames.join(',');

    for ( let i = 0; i < this.filteredNames.length; i++ ) {
      for ( let j = 0; j < this.newArray.length; j++ ) {
       if (this.newArray[j].category === this.filteredNames[i].name) {
         this.newArray[j].selected = true;
       }
      }
    }
    this.newfilteredNames = this.newArray.filter(n1 => n1.selected);
    }*/



/*NG-A6#1 task 1.3  */
  /*names = ['Jack', 'Steve', 'William', 'Kathy', 'Edward'];
  namesIds = [
    {name: 'Jack', id: 'AB455'},
    {name: 'Steve', id: 'GM072'},
    {name: 'William', id: 'CX499'},
    {name: 'Kathy', id: 'MM746'},
    {name: 'Edward', id: 'KT108'},
  ];

  namesStructure = null;
  namesSelected = '';
  idSelected = '';

  ngOnInit() {
  this.updatedValues();
  }

  updatedValues() {
  this.namesStructure = this.namesIds.map(n1 => ({
  name: n1.name,
  id: n1.id,
  selected: false
  }));
  let sIds = this.idSelected.split(',');
  for (let i = 0; i < sIds.length; i++ ) {
    let item = this.namesStructure.find(n1 => n1.id === sIds[i]);
    if (item) {
      item.selected = true;
    }
  }
  }

  optChange() {
  let filteredNames = this.namesStructure.filter(n1 => n1.selected);
  let arrayIds = filteredNames.map(n1 => n1.id);
  this.idSelected = arrayIds.join(',');
  }*/




  /*Angular Set AS-N1#9A code */
 /*questions = [{text: 'What is the capital of India',
  options: ['New Delhi', 'London', 'Paris', 'Tokyo'], answer: 1},
  {text: 'What is the capital of Italy',
  options: ['Berlin', 'London', 'Rome', 'Paris'], answer: 3},
  {text: "What is the capital of China",
  options: ["Shanghai", "HongKong", "Shenzen", "Beijing"], answer: 4},
  {text: "What is the capital of Nepal",
  options: ["Tibet", "Kathmandu", "Colombo", "Kabul"], answer: 2},
  {text: "What is the capital of Iraq",
  options: ["Baghdad", "Dubai", "Riyadh", "Teheran"], answer: 1},
  {text: "What is the capital of Bangladesh",
  options: ["Teheran", "Kabul", "Colombo", "Dhaka"], answer: 4},
  {text: "What is the capital of Sri Lanka",
  options: ["Islamabad", "Colombo", "Maldives", "Dhaka"], answer: 2},
  {text: "What is the capital of Saudi Arabia",
  options: ["Baghdad", "Dubai", "Riyadh", "Teheran"], answer: 1},
  {text: "What is the capital of France",
  options: ["London", "New York", "Paris", "Rome"], answer: 3},
  {text: "What is the capital of Italy",
  options: ["Berlin","London", "Paris", "Rome"], answer: 4},
  {text: "What is the capital of Sweden",
  options: ["Helsinki", "Stockholm", "Copenhagen", "Lisbon"], answer: 2},
  {text: "What is the currency of UK",
  options: ["Dollar", "Mark", "Yen", "Pound"], answer: 4},
  {text: "What is the height of Mount Everest",
  options: ["9231 m", "8848 m", "8027 m", "8912 m"], answer: 2},
  {text: "What is the capital of Japan",
options: ["Beijing", "Osaka", "Kyoto", "Tokyo"], answer: 4},
{text: "What is the capital of Egypt",
options: ["Cairo", "Teheran", "Baghdad", "Dubai"], answer: 1}
];*/


































//////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

/*newData = [
  {
    subject: "General Knowledge",
    name: "4A",
    questions: [
      {
        text: "What is the capital of India",
        options: ["New Delhi", "London", "Paris", "Tokyo"],
        answer: 1
      },
      {
        text: "What is the capital of Italy",
        options: ["Berlin", "London", "Rome", "Paris"],
        answer: 3
      },
      {
        text: "What is the capital of China",
        options: ["Shanghai", "HongKong", "Shenzen", "Beijing"],
        answer: 4
      },
      {
        text: "What is the capital of Nepal",
        options: ["Tibet", "Kathmandu", "Colombo", "Kabul"],
        answer: 2
      },
      {
        text: "What is the capital of Iraq",
        options: ["Baghdad", "Dubai", "Riyadh", "Teheran"],
        answer: 1
      },
      {
        text: "What is the capital of Bangladesh",
        options: ["Teheran", "Kabul", "Colomdo", "Dhaka"],
        answer: 4
      },
      {
        text: "What is the capital of Sri Lanka",
        options: ["Islamabad", "Colombo", "Maldives", "Dhaka"],
        answer: 2
      },
      {
        text: "What is the capital of Saudi Arabia",
        options: ["Baghdad", "Dubai", "Riyadh", "Teheran"],
        answer: 1
      },
      {
        text: "What is the capital of France",
        options: ["London", "New York", "Paris", "Rome"],
        answer: 3
      },
      {
        text: "What is the capital of Germany",
        options: ["Frankfurt", "Budapest", "Prague", "Berlin"],
        answer: 4
      },
      {
        text: "What is the capital of Sweden",
        options: ["Helsinki", "Stockholm", "Copenhagen", "Lisbon"],
        answer: 2
      },
      {
        text: "What is the currency of UK",
        options: ["Dollar", "Mark", "Yen", "Pound"],
        answer: 4
      },
      {
        text: "What is the height of Mount Everest",
        options: ["9231 m", "8848 m", "8027 m", "8912 m"],
        answer: 2
      },
      {
        text: "What is the capital of Japan",
        options: ["Beijing", "Osaka", "Kyoto", "Tokyo"],
        answer: 4
      },
      {
        text: "What is the capital of Egypt",
        options: ["Cairo", "Teheran", "Baghdad", "Dubai"],
        answer: 1
      }
    ]
  },
  {
    subject: "Maths",
    name: "10C",
    questions: [
      {
        text: "What is 8 * 9",
        options: ["72", "76", "64", "81"],
        answer: 1
      },
      {
        text: "What is 2*3+4*5",
        options: ["70", "50", "26", "60"],
        answer: 3
      }
    ]
  },
  {
    subject: "Chemistry",
    name: "7A(i)",
    questions: [
      {
        text: "What is the melting point of ice",
        options: ["0F", "0C", "100C", "100F"],
        answer: 2
      },
      {
        text: "What is the atomic number of Oxygen",
        options: ["6", "7", "8", "9"],
        answer: 3
      },
      {
        text: "What is the atomic number of Carbon",
        options: ["6", "7", "8", "9"],
        answer: 1
      },
      {
        text: "Which of these is an inert element",
        options: ["Flourine", "Suphur", "Nitrogen", "Argon"],
        answer: 4
      },
      {
        text: "What is 0 Celsius in Fahrenheit",
        options: ["0", "32", "20", "48"],
        answer: 2
      }
    ]
  },
  {
    subject: "Computers",
    name: "3B",
    questions: [
      {
        text: "How many bytes are there in 1 kilobyte",
        options: ["16", "256", "1024", "4096"],
        answer: 3
      },
      {
        text: "Who developed ReactJS",
        options: ["Facebook", "Google", "Microsoft", "Apple"],
        answer: 1
      },
      {
        text: "Angular is supported by ",
        options: ["Facebook", "Google", "Microsoft", "Twitter"],
        answer: 2
      },
      {
        text: "C# was developed by ",
        options: ["Amazon", "Google", "Microsoft", "Twitter"],
        answer: 3
      },
      {
        text: "Bootstrap was developed by ",
        options: ["Apple", "Google", "Facebook", "Twitter"],
        answer: 4
      },
      {
        text: "AWS is provided by ",
        options: ["Apple", "Amazon", "Microsoft", "Google"],
        answer: 2
      },
      {
        text: "Azure is provided by ",
        options: ["Microsoft", "Amazon", "IBM", "Google"],
        answer: 1
      },
      {
        text: "Angular is a framework that uses ",
        options: ["Java", "Python", "C#", "Typescript"],
        answer: 4
      }
    ]
  }
];

questions = [];
question = [];
index = 0;
quesno = 1;
condition = true;
Property = false;
Property1 = false;
Property2 = true;
Property3 = false;
Condition1 = true;
Condition2 = true;
name = '';
assignment = '';
checkProperty0 = false;
checkProperty = false;
checkAns = 0;
newIndex = -1;
count = 0;
checkIndex = -1;

do(n) {
  this.quesno = 1;
  this.index = 0;
  this.newIndex = n;
  this.checkAns = 0;
  this.condition = false;
  this.Property = true;
  this.Condition1 = true;
  this.Condition2 = true;
  this.questions = this.newData[n].questions;
  this.name = this.newData[n].subject;
  this.assignment = this.newData[n].name;
  for (let i = 0; i < this.questions.length; i++ ) {
    this.questions[i]['stdAnswer'] = 'Not Answered';
    if (this.questions[i].answer === 1) {
      this.questions[i].answer = 'A';
    }
    if (this.questions[i].answer === 2) {
      this.questions[i].answer = 'B';
    }
    if (this.questions[i].answer === 3) {
      this.questions[i].answer = 'C';
    }
    if (this.questions[i].answer === 4) {
      this.questions[i].answer = 'D';
    }
  }
  this.question = this.questions[this.index];
  console.log(this.question);
  for (let i = 0; i < this.newData.length; i++ ) {
    this.newData[i].questions['stdAnswer'] = 'Not Answered';
  }

}

previous() {
this.question = [];
this.Property2 = true;
this.index = this.index - 1;
this.quesno = this.quesno - 1;
this.question = this.questions[this.index];
console.log(this.question);
if (this.index === 0) {
  this.Property1 = false;
    }
}

next() {
  this.question = [];
  this.Property1 = true;
  this.index = this.index + 1;
  this.quesno = this.quesno + 1;
  this.question = this.questions[this.index];
  console.log(this.question);
  if (this.index === this.questions.length - 1) {
this.Property2 = false;
  }
}

viewMarkSheet() {
this.Property3 = true;
this.Condition1 = false;
this.Condition2 = false;
}

editAnswer(s) {
this.questions[this.index].stdAnswer = s;
}

changeAnswer(j) {
this.index = j;
console.log(this.index);
this.question = this.questions[this.index];
this.quesno = this.index + 1;
this.Property3 = false;
this.Condition1 = true;
this.Condition2 = true;
this.Property = true;
}

submitAss() {
  this.checkIndex = -1;
  this.count = 0;
this.checkProperty = true;
this.checkProperty0 = true;
this.Property3 = false;
this.Condition1 = false;
this.Condition2 = false;
this.Property = false;
this.condition = true;
this.newData[this.newIndex].checkBtn = true;

for (let i = 0; i < this.newData[this.newIndex].questions.length; i++) {
  if (this.newData[this.newIndex].questions[i].answer ===
    this.newData[this.newIndex].questions[i].stdAnswer) {
this.count ++ ;
    }
}
}

check(n) {
this.checkIndex = n;
  this.index = 0;
  this.quesno = 1;
this.checkAns = 1;
this.condition = false;
  this.Property = true;
  this.Condition2 = true;
  this.Condition1 = true;
  this.questions = this.newData[n].questions;
  console.log(n);
  console.log(this.questions);
  this.name = this.newData[n].subject;
  this.assignment = this.newData[n].name;
  this.question = this.questions[this.index];
  console.log(this.question);

}

ngOnInit() {
  for (let i = 0; i < this.newData.length; i++ ) {
    this.newData[i]['checkBtn'] = false;
  }
}*/



  /*this.question.push(this.questions[this.index]);
  console.log(this.question);
  for (let i = 0; i < this.questions.length; i++ ) {
    this.questions[i]['stdAnswer'] = 'Not Answered';
  }
  console.log(this.questions);*/
//}





/*names = ['Jack', 'Steve', 'William', 'Kathy', 'Edward'];
namesIds = [
  {name: 'Jack', id: 'AB455'},
  {name: 'Steve', id: 'GM072'},
  {name: 'William', id: 'CX499'},
  {name: 'Kathy', id: 'MM746'},
  {name: 'Edward', id: 'KT108'},
];

namesStructure = null;
namesSelected = 'Steve,Jack';
idSelected = '';

ngOnInit() {
this.updatedValues();
}

updatedValues() {
this.namesStructure = this.names.map(n1 => ({
name: n1,
selected: false
}));
let sNames = this.namesSelected.split(',');
for (let i = 0; i < sNames.length; i++ ) {
  let item = this.namesStructure.find(n1 => n1.name === sNames[i]);
  if (item) {
    item.selected = true;
  }
}
}

optChange() {
let filteredNames = this.namesStructure.filter(n1 => n1.selected);
let arrayNames = filteredNames.map(n1 => n1.name);
this.namesSelected = arrayNames.join(',');
}*/


  /*NG-A5#4 task 2.2*/
/*students = [
  {name: 'James', course: 'Angular'},
  {name: 'Mary', course: 'Python'},
  {name: 'Bob', course: 'Angular'},
  {name: 'Pam', course: 'Android'},
  {name: 'Steve', course: 'Angular'},
  {name: 'William', course: 'Python'},
  {name: 'Julia', course: 'Android'},
   {name: 'Matt', course: 'Java'},
   {name: 'Martin', course: 'Java'},
    {name: 'Katherine', course: 'Angular'},
    {name: 'Nick', course: 'Python'},
   {name: 'Tim', course: 'Java'},
];
courses = [ 'Angular', 'Android', 'Python', 'Java'];
selcourse = [];
course ;

fn() {
this.selcourse = [...this.students];

}

/*event(s: string) {
this.selcourse = [...this.students];
if (s === 'All') {
  this.selcourse = [...this.students];
}
else{
this.selcourse = this.students.filter(function(a) {
 if ( a.course === s ) {
return a;
  }

});
}
}*/



  /*NG-A5#4 task 1*/
/*courses =  [ 'Angular', 'Java', 'JavaScript', 'React', 'Python', 'PHP', 'Android',
 'Angular', 'Perl', 'jQuery', 'ReactNative'];
selcourse = ['Angular', 'Java', 'JavaScript', 'React', 'Python', 'PHP', 'Android',
'Angular', 'Perl', 'jQuery', 'ReactNative'];
 course = '';

focus1(s: string) {
   this.selcourse = [...this.courses];
   if (s.length > 0) {
this.selcourse = this.courses.filter(function(a) {
  a = a.toLowerCase();
  s = s.toLowerCase();
  if ( a.substring(0, s.length) === s ) {
return a;
  }
});
   }
 }*/



/*NG-A5#3*/
/*username: string = '';
pswd: string = '';
msg1 = '';
msg2 = '';
condition1 = false;
condition2 = false;

focus1() {
  this.condition1 = false;
}

focusout1() {
if (this.username.length < 1) {
this.msg1 = 'Please enter your Email Id/Username';
this.condition1 = true;
}
console.log(this.msg1);
}

focus2() {
  this.condition2 = false;
}

focusout2() {
  if (this.pswd.length < 1) {
    this.msg2 = 'Please enter your Password';
    this.condition2 = true;
    }
    console.log(this.msg2);
}*/



/*NG-A5#2 task 1,2  */
/*courses = ['Angular', 'Python', 'Java', 'Spring Boot'];

course = '';
working;
options;

event(s: string) {
  console.log(s);
}*/




  /*NG-A5#1 task 2.2 */
/*  str = '';
  message = '';
  Property = false;
  event() {
    this.Property = true;
    this.message = '';
    if (this.str.length < 8) {
    this.message = 'Minimum length is 8 characters';
    }

  }

  focus() {
    this.Property = false;
  }*/



/*NG-A5#1 task 2.1 */
/*str = '';
len = 0;
Property = false;
event() {
  this.Property = true;
  this.len = this.str.length;
}

focus() {
  this.Property = false;
}*/



  /*NG-A5#1 task 1*/
/*  event(s: string) {
    console.log(s);
  }*/




  /*


Animals = [
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\croc.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\gorilla.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\giraffe.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\polar-bear.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\elephant.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\whale.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\tiger.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\croc.svg'},
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\gorilla.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\tiger.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\polar-bear.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\elephant.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\whale.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\koala.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\giraffe.svg' },
  { open: 'false', fieldImage: 'C:\Users\user\Pictures\koala.svg' },
];

open1 = -1;
open2 = -1;*/




  /*selCity: string = null;
  selCountry: City = null;
  citiesArray: City[] = cityList;*/


  /*NG-A4#5 task 3*/
/*  selCourse: Student = null;
myStudents: Student[] = studentList;*/


  /*NG-A4#5 task 1*/
/*  selCourse: Course = null;
  myCourses: Course[] = courseList;*/

  /*NG-A4#4 task 2.2 */
  /*startingYear: number ;
  noOfYear: number;
  year: number;

  array = [];

  configure() {
    this.array = [];
    var num = +(this.startingYear);
for ( let i = 0; i < this.noOfYear; i++) {
this.array.push(num + i);
}
  }*/

/*NG-A4#4 task 1 */
/*course: string;
courses = [ 'Java' , 'Angular', 'JavaScript', 'Boot Strap'];*/



/*NG-A4#3 task 2.2  */
  /*joiningcourse: number = 0;
  leavingcourse: number = 0;*/


  /*NG-A4#2*/
/*  working:boolean;
  gender:string='';*/

/*NG-A4#2 task 2.2*/
/*  working: string;*/


/*AS-N1#8*/
/*array = [
   {value: -1, data: ''},
   {value: -1, data: ''},
   {value: -1, data: ''},
   {value: -1, data: ''},
   {value: -1, data: ''},
   {value: -1, data: ''},
   {value: -1, data: ''},
   {value: -1, data: ''},
   {value: -1, data: ''},
];

sign = 'X';
count = 0;
changedvalue(val) {
this.array[val].data = this.sign;
if (this.sign === 'X') {
  this.array[val].value = 0;
this.sign = '0';
}
else {
  this.array[val].value = 1;
  this.sign = 'X';
}
this.count = 0;
for (let i = 0; i < 9 ; i++ ) {
if (this.array[i].value === -1) {
this.count ++;
}
}
if (this.count === 0) {
document.getElementById('h4').innerHTML = 'Game Over';
document.getElementById('btn').innerHTML = 'New Game';

}
}
reset() {
  for (let i = 0; i < 9 ; i++ ) {
    this.array[i].data = '' ;
    this.array[i].value = -1;

  }
  this.sign = 'X';
  //document.getElementById('h4').innerHTML = 'Move :' + this.sign ;
    document.getElementById('btn').innerHTML = 'Reset Game';
}*/

 /* ngOnInit() {

  }


mobiles*/

  /*NG-A3#6H*/
  /*Property = false;
  condition = false;
  UpdatedStudents = Students;

  addForm() {
    this.Property = true;
    this.condition = false;
  }
  studentList() {
    this.condition = true;
    this.Property = false;
  }

  enterData(data) {

    this.UpdatedStudents.students.push(data);
    console.log(this.UpdatedStudents.students);

  }*/

  /*NG-A3#6F*/
  /*products = [
    { name: 'Coca Cola', code: 'c332', price: 20, quantity: 10 },
    { name: '5Star', code: 'F168', price: 15, quantity: 7 },
    { name: 'Maggi', code: 'M228', price: 28, quantity: 16 },
    { name: 'Pepsi', code: 'P228', price: 20, quantity: 18 },
    { name: 'Dairy Milk', code: 'D311', price: 25, quantity: 8 },
    { name: 'Mirinda', code: 'M301', price: 20, quantity: 10 },
    { name: 'Kitkat', code: 'K477', price: 16, quantity: 7 },
    { name: 'Red Bull', code: 'R544', price: 90, quantity: 3 },
  ];

  tableType = 'Table';
  tableCondition = true;
  Property = true;

  updateData() {

  }

  orderByQuantity() {
    this.products.sort(function(a, b) {
     return a.quantity - b.quantity;
    });
  }

  orderByPrice() {
    this.products.sort(function(a, b) {
      return a.price - b.price;
     });
  }

  viewData() {
    if(this.tableType === 'Table') {
this.tableType = 'Grid';
    }
    else{
      this.tableType = 'Table';
    }
    this.Property = !this.Property ;
  }/*



 /* NG-A3#6E  */

  /*mobiles = [
{ name: 'Redmi 6(Upto 64GB)', details: 'Dual Rear Cam|3000mAh', price: 7499 },
{ name: 'RealMe 6(Upto 4GB)', details: 'Dual Rear Cam|4320mAh', price: 8999 },
{ name: 'Honor 7S(2GB|16GB)', details: 'Face Unlock|3020mAh', price: 5499 },
{ name: 'Samsung J6(4GB|64GB)', details: '14.22 cm|Face Unlock', price: 9490 },
{ name: 'Moto One(4GB|64GB)', details: 'Extra 2000 off on Exchange', price: 13999 },
{ name: 'Nokia 6.1(4GB|64GB)', details: 'Full HD Display|Face Unlock', price: 6999 },
  ];

  selMobiles = [];
  Property = false;
  totalAmount = 0;
  noOfItems = 0;

  add(value) {
    var count = 0;
    this.totalAmount = 0;
    this.noOfItems = 0;
    this.Property = true;
    for (let i = 0; i < this.selMobiles.length; i++) {
      if (this.selMobiles[i].name === value.name) {
     count++;
      }
    }
    if (count === 0) {
  this.selMobiles.push(value);
    }
    else {
      value.quantity = value.quantity + 1;
    }
    for (let i = 0; i < this.selMobiles.length; i++) {
this.noOfItems = this.noOfItems + this.selMobiles[i].quantity;
    this.totalAmount = this.totalAmount + (this.selMobiles[i].price * this.selMobiles[i].quantity);
     }
  console.log(this.selMobiles);

  }

  remove(num) {
    this.totalAmount = 0;
    this.noOfItems = 0;
    this.selMobiles.splice(num, 1);
    for (let i = 0; i < this.selMobiles.length; i++) {
      this.noOfItems = this.noOfItems + this.selMobiles[i].quantity;
      this.totalAmount = this.totalAmount + this.selMobiles[i].price;

    }
    if (this.selMobiles.length < 1) {
      this.Property = false;
    }
  }

  ngOnInit() {

    }*/

  /*NG-A3#6D task 1.3 */
  /*books: Book[] = bookdb;
newArray = [];
Property = false;
condition = true;
noOfBooks = 0;
noOfIssuedBooks = 0;

  ngOnInit() {
this.noOfBooks = this.books.length;
this.noOfIssuedBooks = this.newArray.length;
  }
  enterBook(n) {
    this.newArray.push(this.books[n]);
   this.books.splice(n, 1);
   this.noOfBooks = this.books.length;
   this.noOfIssuedBooks = this.newArray.length;
     this.Property = true;
     if (this.books.length < 0) {
       this.condition = false;
     }
  }

  return(j) {
    this.books.push(this.newArray[j]);
    this.newArray.splice(j,1);
    this.noOfBooks = this.books.length;
    this.noOfIssuedBooks = this.newArray.length;
    if (this.newArray.length < 1) {
      this.Property = false;
    }
  }*/




  /*questions: Question[] = questiondb;
  questionNumber: number = 0;
  tempOptions = [];
  tempQuestion = "";
  tempAnswer = -1;

  participants = [
    { name: "James", score: 0 },
    { name: "Julia", score: 0 },
    { name: "Martha", score: 0 },
    { name: "Steve", score: 0 },
  ];

  Property = true;
  participatedno = -1;
  result = "";
  colorno = "";


  ngOnInit() {
    this.tempQuestion = this.questions[0].text;
    this.tempOptions = this.questions[0].options;
    this.tempAnswer = this.questions[0].answer;
    console.log(this.tempOptions);
  }

  buzzer(i: number) {
this.colorno="id"+i;
  document.getElementById(this.colorno).style.backgroundColor="green";


    this.participatedno = i;

  }

  selectedAnswer(j: number) {
    console.log(j);
    if (this.participatedno > -1) {
      if (this.questionNumber < 6) {
        if (j + 1 === this.tempAnswer) {
          alert("Correct Answer. You get 3 points");
          this.participants[this.participatedno].score =
            this.participants[this.participatedno].score + 3;
        } else {
          alert("Wrong Answer. You lose 1 point");
          this.participants[this.participatedno].score =
            this.participants[this.participatedno].score - 1;
        }

        if (this.questionNumber === 5) {
          this.Property = false;
        } else {
          this.questionNumber++;
          this.tempQuestion = this.questions[this.questionNumber].text;
          this.tempOptions = this.questions[this.questionNumber].options;
          this.tempAnswer = this.questions[this.questionNumber].answer;
          document.getElementById(this.colorno).style.backgroundColor="yellow";
          this.participatedno = -1;
        }
      }
    }
  }

  finalResult() {
    this.participants.sort(function (a, b) {
      return b.score - a.score;
    });

    console.log(this.participants);
    var count = 0;
    var names = "";
    for (let i = 1; i < this.participants.length; i++) {
      if (this.participants[0].score === this.participants[i].score) {
        var names = names + "," + this.participants[i].name;
        count = count + 1;
      }
    }
    if (count === 0) {
      this.result = "The winner is " + this.participants[0].name;
      return this.result;
    } else {
      this.result =
        "There is a Tie. The winners are " + this.participants[0].name + names;
      return this.result;
    }
  }*/



  /*pics= ["https://images.pexels.com/photos/356378/pexels-photo-356378.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/39317/chihuahua-dog-puppy-cute-39317.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/1254140/pexels-photo-1254140.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/58997/pexels-photo-58997.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/33053/dog-young-dog-small-dog-maltese.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/374898/pexels-photo-374898.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/1490908/pexels-photo-1490908.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/551628/pexels-photo-551628.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/1629781/pexels-photo-1629781.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/545063/pexels-photo-545063.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  "https://images.pexels.com/photos/257540/pexels-photo-257540.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
];

picArray = [];
favouriteArray = [];
picNumber = 0;
Property = false;

  ngOnInit() {
   this.picArray.push(this.pics[0]);


  }

previousPhoto() {
if (this.picNumber > 0){
this.picNumber--;
this.picArray=[];
this.picArray.push(this.pics[this.picNumber]);
}
}

nextPhoto() {
  if (this.picNumber < this.pics.length-1)
  this.picNumber++;
  this.picArray=[];
  this.picArray.push(this.pics[this.picNumber]);

  }

  newChange(){
    this.Property = true;
    if(this.favouriteArray.length<1){
this.favouriteArray.push(this.pics[this.picNumber]);
    }
    else{
      var count = 0;
      for(let i = 0; i< this.favouriteArray.length; i++){
        if(this.favouriteArray[i] === this.pics[this.picNumber] ){
          count++;
        }
      }
      if( count === 0 ) {
        this.favouriteArray.push(this.pics[this.picNumber]);
      }
    }
  }

  removePic(ele){
    console.log("jkerb");
    var index = this.favouriteArray.indexOf(ele);
    console.log(index);
    this.favouriteArray.splice(index,1);
    if(this.favouriteArray.length<1){
      this.Property = false;
    }
  }*/


 /*ngOnInit() {
    var n = 20;
   var m = 3;
          var res = '';
          for (let i = n; i > 1; i++) {

          res = res + n + ',';
           n = n - m;
          }

          console.log(res);
 }*/




}
