export class Book {
  name: string;
  author: string;
description: string;
image: string;
}


/*export let bookdb: Book[] = [
  {name: 'Harry Potter',
  author: 'J.K.Rowling',
  description: 'Harry Potter is a series of fantasy novels written by British author J. K. Rowling. The novels chronicle the lives of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, all of whom are students at Hogwarts School of Witchcraft and Wizardry. The main story arc concerns Harrys struggle against Lord Voldemort, a dark wizard who intends to become immortal, overthrow the wizard governing body known as the Ministry of Magic and subjugate all wizards and Muggles (non-magical people).',
  image: 'https://images.gr-assets.com/polls/1380398422p7/91309.jpg'},

  {name: 'War and Peace',
  author: 'Leo Tolstoy',
  description: "War and Peace is a novel by the Russian author Leo Tolstoy. It is regarded as a central work of world literature and one of Tolstoy finest literary achievements. The novel chronicles the history of the French invasion of Russia and the impact of the Napoleonic era on Tsarist society through the stories of five Russian aristocratic families. Portions of an earlier version, titled The Year 1805,[4] were serialized in The Russian Messenger from 1865 to 1867. The novel was first published in its entirety in 1869.",
  image: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1384803290l/18626867.jpg'},

  {name: "Malgudi Days",
  author: "RK Narayan",
  description:"Malgudi Days is a collection of short stories by R. K. Narayan published in 1943 by Indian Thought Publications. The book was republished outside India in 1982 by Penguin Classics. The book includes 32 stories, all set in the fictional town of Malgudi, located in South India.",
  image:"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1455574981l/14082.jpg"},

  {name: "Gitanjali",
  author: "RK Tagore",
  description:"Gitanjali is a collection of poems by the Bengali poet Rabindranath Tagore. Tagore received the Nobel Prize for Literature, largely for the book. It is part of the UNESCO Collection of Representative Works. Its central theme is devotion & motto is I am here to sing thee songs.",
  image:"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1385265973l/18922254.jpg"},

  {name: "Tale of Two Cities",
  author: "Charles Dickens",
  description:"A Tale of Two Cities (1859) is a historical novel by Charles Dickens, set in London and Paris before and during the French Revolution. The novel tells the story of the French Doctor Manette, his 18-year-long imprisonment in the Bastille in Paris and his release to live in London with his daughter Lucie, whom he had never met. The story is set against the conditions that led up to the French Revolution and the Reign of Terror.",
  image:"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1344922523l/1953.jpg"},


  {name: "Three Musketeers",
  author: "Alexander Dumas",
  description:"The Three Musketeers is a historical adventure novel written in 1844 by French author Alexandre Dumas. Situated between 1625 and 1628, it recounts the adventures of a young man named dArtagnan after he leaves home to travel to Paris, to join the Musketeers of the Guard. Although dArtagnan is not able to join this elite corps immediately, he befriends the three most formidable musketeers of the age –Athos, Porthos and Aramis, the three inseparables, as these are called –and gets involved in affairs of the state and court.",
  image:"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1320436982l/7190.jpg"}
];*/

export let bookdb: Book[] = [
  {name: 'Harry Potter',
  author: 'J.K.Rowling',
  description: 'Harry Potter is a series of fantasy novels written by British author J. K. Rowling. The novels chronicle the lives of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, all of whom are students at Hogwarts School of Witchcraft and Wizardry. The main story arc concerns Harrys struggle against Lord Voldemort, a dark wizard who intends to become immortal, overthrow the wizard governing body known as the Ministry of Magic and subjugate all wizards and Muggles (non-magical people).',
  image: 'https://images.gr-assets.com/polls/1380398422p7/91309.jpg'},

  {name: 'War and Peace',
  author: 'Leo Tolstoy',
  description: "War and Peace is a novel by the Russian author Leo Tolstoy. It is regarded as a central work of world literature and one of Tolstoy finest literary achievements. The novel chronicles the history of the French invasion of Russia and the impact of the Napoleonic era on Tsarist society through the stories of five Russian aristocratic families. Portions of an earlier version, titled The Year 1805,[4] were serialized in The Russian Messenger from 1865 to 1867. The novel was first published in its entirety in 1869.",
  image: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1384803290l/18626867.jpg'},

  {name: "Malgudi Days",
  author: "RK Narayan",
  description:"Malgudi Days is a collection of short stories by R. K. Narayan published in 1943 by Indian Thought Publications. The book was republished outside India in 1982 by Penguin Classics. The book includes 32 stories, all set in the fictional town of Malgudi, located in South India.",
  image:"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1455574981l/14082.jpg"},

  {name: "Gitanjali",
  author: "RK Tagore",
  description:"Gitanjali is a collection of poems by the Bengali poet Rabindranath Tagore. Tagore received the Nobel Prize for Literature, largely for the book. It is part of the UNESCO Collection of Representative Works. Its central theme is devotion & motto is I am here to sing thee songs.",
  image:"https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1385265973l/18922254.jpg"},
];