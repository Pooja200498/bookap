import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

mobileArray = [
  {name: 'Xiaomi', model: 'Redmi 5', price: '7999' },
  {name: 'Samsung', model: 'On6 Pro', price: '10999' },
  {name: 'Oppo', model: 'F16 S', price: '12499' },
  {name: 'Vivo', model: 'V20X', price: '8999' },
  {name: 'Motorola', model: 'G5S', price: '10999' },
];


  constructor(private authService: AuthService,
    private route: ActivatedRoute, private router: Router
    ) { }

  ngOnInit() {

  }

add(n) {
  let data =  this.mobileArray[n];
  console.log(data);
  this.authService.addToCart(data);
  
}

  /*uname = '';
pwd = '';
loginFail = false;

  constructor(private authService: AuthService,
    private route: ActivatedRoute, private router: Router
    ) { }

  ngOnInit() {
  }

  start() {

      this.loginFail = false;
      this.authService.login();

  }*/


  /*visitedPages;
  newArray = [];

  constructor(private authService: AuthService,
    private route: ActivatedRoute, private router: Router
    ) { }


  ngOnInit() {
    this.visitedPages = this.authService.visitedPages;
    this.visitedPages = this.visitedPages + 1;
    this.authService.changedData(this.visitedPages);
    this.authService.changedData1('Mobile');
    this.newArray = this.authService.visitedArray;
  }*/

}
