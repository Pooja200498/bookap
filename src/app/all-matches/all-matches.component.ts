import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-all-matches',
  templateUrl: './all-matches.component.html',
  styleUrls: ['./all-matches.component.css']
})
export class AllMatchesComponent implements OnInit {
@Input() matches;
  constructor() { }

  ngOnInit() {
  }

}
