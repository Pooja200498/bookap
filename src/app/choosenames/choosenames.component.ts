import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-choosenames',
  templateUrl: './choosenames.component.html',
  styleUrls: ['./choosenames.component.css']
})
export class ChoosenamesComponent implements OnInit {
@Input() namesCB;
@Input() namesRadio;
@Output() optSel1 = new EventEmitter();
@Output() optSel2 = new EventEmitter();


  constructor() { }

  emitChange1() {
    this.optSel1.emit();
  }
  emitChange2() {
    this.optSel2.emit();
  }
  ngOnInit() {
  }

}
