import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
@Output() newData = new EventEmitter();
cname = '';
cemail = '';


  constructor() { }

  ngOnInit() {
  }

  submitForm() {
    var data = {
      name : this.cname ,
      email : this.cemail
    }
    console.log(data);
this.newData.emit(data);

  }

}
