import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamoptionsComponent } from './teamoptions.component';

describe('TeamoptionsComponent', () => {
  let component: TeamoptionsComponent;
  let fixture: ComponentFixture<TeamoptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamoptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamoptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
