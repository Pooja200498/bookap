import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialMediaPostComponentComponent } from './social-media-post-component.component';

describe('SocialMediaPostComponentComponent', () => {
  let component: SocialMediaPostComponentComponent;
  let fixture: ComponentFixture<SocialMediaPostComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialMediaPostComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialMediaPostComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
