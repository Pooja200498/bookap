import { SocialMedia } from './../socialmediadata';
import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'app-social-media-post-component',
  templateUrl: './social-media-post-component.component.html',
  styleUrls: ['./social-media-post-component.component.css']
})
export class SocialMediaPostComponentComponent implements OnInit {
@Input() social: SocialMedia;
  constructor() { }

  ngOnInit() {
  }

  addLikes(x: number) {
    this.social.numOfLikes = this.social.numOfLikes + x ;
  }

  addShare(x: number) {
    this.social.numOfShares = this.social.numOfShares + x ;
  }

}
