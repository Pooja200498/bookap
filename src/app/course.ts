export class Course {
id: string;
name: string;
desc: string;
}

export class Student {
  id: string;
  name: string;
  city: string;
  course: string;
}

export let studentList: Student[] = [
{id: '101', name: 'Pooja', city: 'Mathura', course: 'Angular'},
{id: '102', name: 'Parul', city: 'Noida', course: 'Boot Strap'},
{id: '103', name: 'Laxmi', city: 'Greater Noida', course: 'Java'},
{id: '104', name: 'Kritika', city: 'Delhi', course: 'Python'},
];

export let courseList: Course[] = [
  {id: 'NG108', name: 'Angular', desc: 'Rich Web Applications'},
  {id: 'AN141', name: 'Android', desc: 'Mobile Apps'},
  {id: 'SP233', name: 'Spring Boot', desc: 'Java Frameworks'},
 ];

 export class City {
  name: string;
  cities: any[];
}

 export let cityList: City[] = [
   {'name': 'India', 'cities': ['Delhi', 'Bangalore', 'Mumbai', 'Chennai']},
 {'name': 'USA', 'cities': ['New York', 'Seattle', 'SanFrancisco']},
 {'name': 'China', 'cities': ['Beijing', 'Shanghai', 'Shenzen']},
];