import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NetService } from './../net.service';

@Component({
  selector: 'app-bye',
  templateUrl: './bye.component.html',
  styleUrls: ['./bye.component.css']
})
export class ByeComponent implements OnInit {

  url1 = 'http://localhost:2410/sporticons/stars';
  strData;
  data: any;
  newData=[];
    //constructor(private authService : AuthService, netService: NetService) { }
    constructor(private  netService: NetService) { }

    ngOnInit() {
  console.log("abc");
    this.netService.getData(this.url1)
  .subscribe(resp => {
  this.strData = JSON.stringify(resp);
  this.data = resp;
  
  for (let i = 0; i < this.data.length; i++) {
    if (this.data[i].sport === 'Football') {
      this.newData.push(this.data[i]);
    }
       }

  });
  }




/*studentsList = [
  {name: 'Jack', email: 'jack@test.com', course: 'Python'},
  {name: 'Mary', email: 'mary@test.com', course: 'Angular'},
  {name: 'Tim', email: 'Tim@test.com', course: 'React'},
];
name;
email;
course;

constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.name = params.get('name');
      this.email = params.get('email');
      this.course = params.get('course');
      });

      this.studentsList.push({name: this.name, email: this.email, course: this.course});
      console.log(this.studentsList);
  }*/


 /*name: string;
student: string;
nextClass: String = null;
homework: String = null;
msg: String = null;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.name = params.get("username")
      //this.student = params.get("studentname")
    });

this.route.queryParamMap.subscribe(params => {
this.nextClass = params.get('next');
this.homework = params.get('hw');
this.msg = params.get('msg');
});

  }


newMsg() {
  let path = "/bye/" + this.name;
  let myparams = {};
  myparams['msg'] = 'Surprise Quiz';
  if (this.nextClass) {
   myparams['next'] = this.nextClass;
  }

  if (this.homework) {
    myparams['hw'] = this.homework;
   }
  this.router.navigate([path], {queryParams: myparams, queryParamsHandling: 'merge'});
  }*/



/*name: string;
extraClass: string;
faculty: String = null;
time: String = null;
message: String = null;

  constructor(private route: ActivatedRoute, private router: Router) { }
ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.name = params.get("username")
      //this.student = params.get("studentname")
    });

this.route.queryParamMap.subscribe(params => {
this.extraClass = params.get('extra');
this.time = params.get('time');
this.faculty = params.get('faculty');
this.message = params.get('message');
});
}
setMsg() {
  let path = "/bye/" + this.name;
  let myparams = {};
  myparams['message'] = 'Guest Lecture';
 this.router.navigate([path], {queryParams: myparams, queryParamsHandling: 'merge'});
}*/

}
